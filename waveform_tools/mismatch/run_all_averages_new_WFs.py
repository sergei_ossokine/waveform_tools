#!/usr/bin/env python                                                                                                           
import argparse
import os
import subprocess as sp
import uuid
import numpy as np
# Create a function called "chunks" with two arguments, l and n:
def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i:i+n]

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--run_dir",type=str,help="Directory where the results will be output",default=os.getcwd())
    p.add_argument("--case_file",type=str,help="File containing the physical parameters to run on")
    p.add_argument("--signal_approx",type=str,help="Approximant to use for signal")
    p.add_argument("--template_approx",type=str,help="Approximant to use for template")
    p.add_argument("--iota_s",type=float,help="Inclination angle to use",default=np.pi/3)
    args=p.parse_args()
    
            
    run_dir = os.path.abspath(args.run_dir)
    try:
        os.chdir(run_dir)
    except OSError:
        print("Could not go into the run directory {}, quitting!".format(run_dir))
        exit(-1)

    
    script_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    lst = np.genfromtxt(args.case_file,delimiter=",")
    chk = list(chunks(lst,50))
    header = """#!/bin/bash -                                                                                      
#SBATCH -J chunk_{}                 # Job Name                                                                         
#SBATCH -o chunk_{}.stdout          # Output file name                                                
#SBATCH -e chunk_{}.stderr          # Error file name                                               
#SBATCH -n 16                 # Number of cores                                         
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node                                  
#SBATCH -p nr                 # Queue name                                                               
#SBATCH -t 23:0:00           # Run time                                                                     
#SBATCH --no-requeue                                                                                                                   
source {}/bin/activate

cd {}
"""
    unq_name = uuid.uuid4().hex
    root = "/work/sossokine/LVCFormatWaveforms"
    j = 0
    for i in range(len(chk)):
        nm = unq_name+"_{}".format(i)
        fp = open("chunk_{}.sh".format(nm),"w")
        fp.write(header.format(nm,nm,nm,os.environ["ENV_HOME"],run_dir))
        for it in chk[i]:
            cmd = """python {}/compare_wf_models_h2h.py --signal_approx {} --template_approx {} --parameter_file {} --offset {} --iota_s {} \n""".format(script_dir,args.signal_approx,args.template_approx,args.case_file,j,args.iota_s)
            fp.write(cmd)
            j+=1
        fp.close()
        submit_cmd="sbatch chunk_{}.sh".format(nm)
        #print(submit_cmd)
        sp.call(submit_cmd,shell=True)

