#!/usr/bin/env/python3
import argparse
import numpy as np
import sys

import lal

sys.path.append("/home/sossokine/Sources/waveform_tools")
from waveform_tools.mismatch.unfaithfulness import run_unfaithfulness
from joblib import delayed, Parallel


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--signal", type=str, help="Approximant used for signal")
    p.add_argument("--template", type=str, help="Approximant to use for template")
    p.add_argument("--ell_max", type=int, help="Maximum ell to include", default=4)
    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=np.pi / 3
    )
    p.add_argument(
        "--min_type",
        type=str,
        help="For Phenom waveforms whether to minimize over reference frequency or rigid rotation of the spins",
        default="reference_frequency",
    )
    p.add_argument(
        "--unfaithfulness_type",
        type=str,
        help="The type of unfaithfulness to use.",
        default="unfaithfulness_RC",
    )
    p.add_argument("--q", type=float, help="Mass ratio, q>=1")
    p.add_argument(
        "--chi1", type=str, help="Dimensionless spin of primary, comma separated"
    )
    p.add_argument(
        "--chi2", type=str, help="Dimensionless spin of secondary, comma separated"
    )
    p.add_argument(
        "--omega_min",
        type=float,
        help="Starting **orbital** frequency of waveform generation, in geometric units",
    )

    p.add_argument(
        "--eccentricity",
        type=float,
        help="Initial eccentricity of the waveform",
        default=0.0,
    )

    p.add_argument(
        "--mean_anomaly",
        type=float,
        help="Initial mean_anomaly the waveform",
        default=0.0,
    )

    args = p.parse_args()
    chi1 = np.array([float(x) for x in args.chi1.split(",")])
    chi2 = np.array([float(x) for x in args.chi2.split(",")])
    name = f"{args.signal}_{args.template}"
    # We need to convert from omega to freq_1M
    freq_1M = args.omega_min / np.pi / lal.MTSUN_SI
    if args.eccentricity != 0.0 or args.mean_anomaly != 0.0:
        if args.mean_anomaly != 0.0:
            parameters = dict(
                q=args.q,
                chi1=chi1,
                chi2=chi2,
                freq_1M=freq_1M,
                case=name,
                eccentricity=args.eccentricity,
                mean_anomaly=args.mean_anomaly,
            )
        else:
            parameters = dict(
                q=args.q,
                chi1=chi1,
                chi2=chi2,
                freq_1M=freq_1M,
                case=name,
                eccentricity=args.eccentricity,
            )
    else:
        parameters = dict(q=args.q, chi1=chi1, chi2=chi2, freq_1M=freq_1M, case=name)

    phis = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    kappas = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    x, y = np.meshgrid(kappas, phis)
    x = x.flatten()
    y = y.flatten()
    
    Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(run_unfaithfulness)(
            y[i],
            x[i],
            args.iota_s,
            i,
            parameters=parameters,
            ellMax=args.ell_max,
            signal_approx=args.signal,
            template_approx=args.template,
            fhigh=2048.0,
            minimization_type=args.min_type,
            unfaithfulness_type=args.unfaithfulness_type,
        )
        for i in range(len(x))
    )
