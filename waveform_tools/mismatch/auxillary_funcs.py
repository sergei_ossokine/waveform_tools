#!/usr/bin/env python3

import os
import sys
from typing import Dict, Tuple, Union

import lal
import lalsimulation as lalsim
import numpy as np
from pycbc.types import FrequencySeries, TimeSeries
from scipy.optimize import root_scalar
from scipy.signal import argrelmax
from waveform_tools.mismatch.EOB_utils import write_rhOverM_h5_SEOBNR
from waveform_tools.mismatch.waveform_parameters import waveform_params

try:
    from waveform_tools.mismatch.eccentric_waveforms import *
except:
    pass


def _planck_window(t, tol=0.005):
    """f(t) = 0 if t < tol
    = 1 if t > 1 - tol
    = 1/(1 + exp(z)) otherwise, where z = 1/t - 1/(1-t)"""
    safe = (t > tol) * (t < (1.0 - tol))
    # temporarily set unsafe times to 0.5 to avoid dividing by 0
    safeT = safe * t + (1.0 - safe) * 0.5
    safeZ = 1.0 / safeT - 1.0 / (1.0 - safeT)
    return safe * 1.0 / (1.0 + np.exp(safeZ)) + (t >= (1.0 - tol))


def window_function(t, t_s=0.0, t_e=1.0, rolloff=False):
    """Return a Planck window function, evaluated at an array of times t:
    When t <= t_s, f(t) = 0
    When t >= t_e, f(t) = 1
    Otherwise, 0 <= f(t) <= 1, where f is monotonically increasing.

    If rolloff=True, reverse the window so that f(t_s) = 1, f(t_e) = 0.

    This is a modified version of the code from gwtools by C. Galley
    """
    if rolloff:
        return window_function(-t, t_s=-t_e, t_e=-t_s)

    # Rescale the time so that the time interval of interest is always
    # 0 to 1
    scaled_t = (t - t_s) / (t_e - t_s)
    return _planck_window(scaled_t)


def taper_waveform(
    ts: np.ndarray, h: np.ndarray, rollon_end: int = 4, ending_time: float = 35
) -> np.ndarray:
    """Taper a waveform using a Planck window

    Args:
        ts (np.ndarray): The time points
        h (np.ndarray): The signal array
        rollon_end (int, optional): End point of the roll-on interval in terms of maxima. Defaults to 4.
        ending_time (float, optional): Start time of the roll-off point, in whatever units the time array is. Defaults to 35 *in geometric units*.

    Returns:
        np.ndarray: The windowed waveform
    """
    maxs = argrelmax(h, order=20)[0]
    # Roll-on interval
    t_l1 = ts[0]
    t_l2 = ts[maxs[rollon_end]]

    # Roll-off interval
    t_h1 = ts[-1] - ending_time
    t_h2 = ts[-1]

    # Roll-on window
    window1 = window_function(ts, t_s=t_l1, t_e=t_l2)
    # Roll-off window
    window2 = window_function(ts, t_s=t_h1, t_e=t_h2, rolloff=True)
    return h * window1 * window2


def taper_pycbc_series(
    time_series: TimeSeries, rollon_end: int = 8, ending_time: float = 50
) -> TimeSeries:
    """Convenience function to taper a pycbc TimeSeries.

    Args:
        time_series (TimeSeries): The input time series
        rollon_end (int, optional): End point of the roll-on interval in terms of maxima. Defaults to 4.
        ending_time (float, optional): Start time of the roll-off point, in whatever units the time array is. Defaults to 35 *in geometric units*.

    Returns:
        TimeSeries: The windowed TimeSeries
    """
    ts = time_series.sample_times.data
    h = time_series.numpy()
    return TimeSeries(
        taper_waveform(ts, h, rollon_end=rollon_end, ending_time=ending_time),
        delta_t=time_series.delta_t,
    )


def get_padded_length(h):
    N = len(h)
    return int(2 ** (np.floor(np.log2(N)) + 2))


def condition_waveform(t, h, n=None, convert=True):
    # Taper
    h = taper_waveform(t, h)
    # Pad

    if n is None:
        n_pad = get_padded_length(h)
    else:
        n_pad = int(2 ** (np.floor(np.log2(n)) + 2))
    h.resize(n_pad)
    if convert:
        h = TimeSeries(h, delta_t=np.diff(t)[0])
    return h


def condition_pycbc_series(h, n=None):
    # Taper
    h = taper_pycbc_series(h)
    # Pad

    if n is None:
        n_pad = get_padded_length(h)
    else:
        n_pad = int(2 ** (np.floor(np.log2(n)) + 2))
    h.resize(n_pad)
    return h


def Ylm(l: int, m: int, theta: float, phi: float) -> float:
    """Get the spin-2 weighter spherical harmonics

    Args:
        l (int): ell
        m (int): m
        theta (float): inclination angle
        phi (float): azimuthal angle

    Returns:
        float: The value of the Ylm
    """
    return lal.SpinWeightedSphericalHarmonic(theta, phi, -2, l, m)


def combine_modes(
    iota: float, phi: float, modes_dict: Dict
) -> Tuple[np.array, np.array]:
    """Combine modes to compute the waveform polarizations in the direction
    (iota,np.pi/2-phi)

    Args:
        iota (float): Inclination angle (rad)
        phi (float): Azimuthal angle(rad)
        modes_dict (Dict): Dictionary containing the modes, either time of frequency-domain

    Returns:
        np.array: Waveform in the given direction
    """
    sm = 0.0
    for key in modes_dict.keys():
        ell, m = [int(x) for x in key.split(",")]
        sm += Ylm(ell, m, iota, np.pi / 2 - phi) * modes_dict[key]
    return np.real(sm), -np.imag(sm)


def compute_frame_invariant_amplitude(modes_dict: Dict) -> np.array:
    """Compute the frame invariant amplitude, viz
    $A = \sqrt{\sum_{l,m}|h_{lm}|^{2}}$

    Args:
        modes_dict (Dict): dictionary containing modes

    Returns:
        np.array: The frame invariant amplitude
    """
    sm = 0.0
    for mode in modes_dict.keys():
        sm += np.abs(modes_dict[mode]) ** 2
    return np.sqrt(sm)


def get_time_to_merger(t: np.array, modes_dict: Dict) -> float:
    """Return the time to merger measured as
    the time of the peak of the frame invariant amplitude

    Args:
        t (np.array): The time series
        modes_dict (Dict): The modes dictionary

    Returns:
        float: The time to merger
    """
    A = compute_frame_invariant_amplitude(modes_dict)
    idx_max = np.argmax(A)
    t_max = t[idx_max]
    t_min = t[0]
    return t_max - t_min


def tdiff(
    f_min: float, NR_duration: float, params: waveform_params, ell_max: int = 4
) -> float:
    """Compute the difference to the merger time between NR and EOB. Merger time
    is defined as the peak of the frame invariant amplitude.

    Args:
        f_min (float): Starting frequency [Hz]
        NR_duration (float): Duration of the waveform [M]
        params (waveform_params): The parameters of the EOB waveform
        ell_max (int, optional): Max ell to use. Defaults to 4.

    Returns:
        float: The difference in the time to merger
    """
    m1 = params.m1
    m2 = params.m2
    max_freq = lalsim.EOBHighestInitialFreq(params.m1 + params.m2)
    if params.approx == "SEOBNRv4PHMSur":
        min_freq = lalsim.SEOBNRv4PHMSur_lowest_frequency(
            params.m1 * lal.MSUN_SI,
            params.m2 * lal.MSUN_SI,
            params.s1x,
            params.s1y,
            params.s1z,
            params.s2x,
            params.s2y,
            params.s2z,
        )
        if f_min < 1.01 * min_freq:
            f_min = 1.01 * min_freq
    if params.approx == "SEOBNRv4PHM" or params.approx == "SEOBNRv4P":
        if f_min > max_freq:
            f_min = (1 - 1e-4) * max_freq

    params.f_min = f_min
    params.f_ref = f_min

    if params.approx == "TEOBResumSe":
        modesTEOB = [[2, 2]]
        t, hp, hc, hlm = generate_TEOBResumSe_simple_waveform(
            params, max_freq, modesTEOB
        )
        amp22 = hlm["1"][0]
        EOB_duration = get_time_to_merger_nonLAL_22mode(t, amp22)

    elif params.approx == "TEOBResumSeHM":
        modesTEOB = [
            [2, 2],
            [2, 1],
            [3, 3],
            [3, 2],
            [4, 4],
            [5, 5],
        ]  # More reliable modes
        t, hp, hc, hlm = generate_TEOBResumSe_simple_waveform(
            params, max_freq, modesTEOB
        )
        EOB_duration = get_time_to_merger(t, hlm)

    elif params.approx == "SEOBNREv4":
        t, hp, hc, amp22 = generate_SEOBNREv4_waveform(params, max_freq)
        EOB_duration = get_time_to_merger_nonLAL_22mode(t, amp22)
    else:
        hlm_EOB = generate_EOB_modes(params, ell_max)
        tmp = lalsim.SphHarmTimeSeriesGetMode(hlm_EOB, 2, 2)
        time_EOB = tmp.deltaT * np.arange(len(tmp.data.data))
        modes_EOB = {}
        for ell in range(2, ell_max + 1):
            for m in range(-ell, ell + 1):
                modes_EOB[ell, m] = lalsim.SphHarmTimeSeriesGetMode(
                    hlm_EOB, ell, m
                ).data.data
        EOB_duration = get_time_to_merger(time_EOB, modes_EOB)

    EOB_duration = EOB_duration / ((m1 + m2) * lal.MTSUN_SI)
    # print(f_min, NR_duration, EOB_duration, NR_duration - EOB_duration)
    return NR_duration - EOB_duration


# get_time_to_merger_nonLAL_22mode
def get_time_to_merger_nonLAL_22mode(t: np.array, amp22: np.array) -> float:
    """Return the time to merger measured as
    the time of the peak of the 22 mode (no higher order modes)

    Args:
        amp22 (np.array): The time series
    Returns:
        float: The time to merger
    """

    # Compute tPeak from a SEOBNRv4 (e=0) waveform
    idx_max = np.argmax(amp22)
    t_max = t[idx_max]
    t_min = t[0]
    return t_max - t_min


def iterate_time_to_merger(
    params_signal: waveform_params,
    params_template: waveform_params,
    NR_file: str,
    NR_ma: lal.Value,
    ell_max: int = 4,
    f_max: float = 2048.0,
) -> float:
    """Find the starting frequency of EOB such that it has the same time to merger as NR.
    The time to merger is defined as the time of peak of the frame-invariant amplitude

    Args:
        params_signal (waveform_params): The parameters of the NR simulation
        params_template (waveform_params): The parameters of the EOB template

        NR_file (str): The file containing NR data
        NR_ma (lal.ModeArray): The mode array for NR modes

    Returns:
        float: The starting frequency of EOB (Hz)
    """
    print("Iterating time to merger")
    if NR_file:
        _, hlm_NR = lalsim.SimInspiralNRWaveformGetHlms(
            params_signal.delta_t,
            params_signal.m1 * lal.MSUN_SI,
            params_signal.m2 * lal.MSUN_SI,
            params_signal.distance,
            params_signal.f_min,
            0.0,
            params_signal.s1x,
            params_signal.s1y,
            params_signal.s1z,
            params_signal.s2x,
            params_signal.s2y,
            params_signal.s2z,
            NR_file,
            NR_ma,
        )

    elif params_signal.approx == "TEOBResumSeHM":
        modes = [[2, 2], [2, 1], [3, 3], [3, 2], [4, 4], [5, 5]]  # More reliable modes
        t, hp, hc, hlm = generate_TEOBResumSe_simple_waveform(
            params_signal, f_max, modesTEOB
        )

    elif params_signal.approx == "TEOBResumSe":
        modesTEOB = [[2, 2]]
        t, hp, hc, hlm = generate_TEOBResumSe_simple_waveform(
            params_signal, f_max, modesTEOB
        )

    elif params_signal.approx == "SEOBNREv4":
        t, hp, hc, amp22 = generate_SEOBNREv4_waveform(params_signal, f_max)

    elif params_signal.approx == "SEOBNRv4":
        nqcCoeffsInput = lal.CreateREAL8Vector(
            10
        )  ##This will be unused, but it is necessary

        # print([params_signal.delta_t, params_signal.m1 * lal.MSUN_SI, params_signal.m2 * lal.MSUN_SI, params_signal.f_min, params_signal.distance, params_signal.s1z,   params_signal.s2z, 41, 0., 0., 0.,0.,0.,0.,0.,0.,1.,1.,nqcCoeffsInput, 0])
        sphtseries, dyn, dynHi = lalsim.SimIMRSpinAlignedEOBModes(
            params_signal.delta_t,
            params_signal.m1 * lal.MSUN_SI,
            params_signal.m2 * lal.MSUN_SI,
            1.0 * params_signal.f_min,
            params_signal.distance,
            params_signal.s1z,
            params_signal.s2z,
            41,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            1.0,
            nqcCoeffsInput,
            0,
        )

        h22 = sphtseries.next.next.next.next.mode.data.data  # This is h_22
        t = np.arange(0, len(h22) * params_signal.delta_t, params_signal.delta_t)
        amp22 = np.abs(h22)

    else:
        print("params_signal.approx  =  ", params_signal.approx)
        lal_dict = lal.CreateDict()
        if NR_ma:
            lalsim.SimInspiralWaveformParamsInsertModeArray(lal_dict, NR_ma)
        hlm_NR = lalsim.SimInspiralChooseTDModes(
            0.0,
            params_signal.delta_t,
            params_signal.m1 * lal.MSUN_SI,
            params_signal.m2 * lal.MSUN_SI,
            params_signal.s1x,
            params_signal.s1y,
            params_signal.s1z,
            params_signal.s2x,
            params_signal.s2y,
            params_signal.s2z,
            params_signal.f_min,
            params_signal.f_ref,
            params_signal.distance,
            lal_dict,
            ell_max,
            lalsim.GetApproximantFromString(params_signal.approx),
        )

    if params_template.approx == "SEOBNRv4PHM" and ell_max < 6:
        ell_max = ell_max
    elif params_template.approx == "SEOBNRv4P":
        ell_max = 2
    elif params_template.approx == "SEOBNRv3":
        ell_max = 2
    elif params_template.approx == "SEOBNRv4":
        ell_max = 2

    if params_signal.approx == "TEOBResumSe":
        amp22 = hlm["1"][0]
        NR_duration = get_time_to_merger_nonLAL_22mode(t, amp22)
        NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI
    elif params_signal.approx == "SEOBNREv4":
        NR_duration = get_time_to_merger_nonLAL_22mode(t, amp22)
        NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI
        # print(params_signal.approx)
    elif params_signal.approx == "TEOBResumSeHM":
        # NR_duration = get_time_to_merger_nonLAL_22mode(t, hlm, modes)
        NR_duration = get_time_to_merger(t, hlm)
        NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI
        # print(params_signal.approx)

    elif params_signal.approx == "SEOBNRv4":
        NR_duration = get_time_to_merger_nonLAL_22mode(t, amp22)
        NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI

    else:

        """I think the following code may not be necessary ..."""

        if (
            params_template.approx == "SEOBNREv4"
            or params_template.approx == "TEOBResumSe"
        ):
            # non LAL waveform (only 22 waveform)
            modes_NR = {}
            modes_NR[2, 2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2).data.data
            modes_NR[2, -2] = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, -2).data.data

            tmp = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2)
            time_NR = tmp.deltaT * np.arange(len(tmp.data.data))
            NR_duration = get_time_to_merger(time_NR, modes_NR)
            NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI

        elif (
            params_template.approx != "SEOBNRv4P"
            or params_template.approx != "SEOBNRv4PHM"
            or params_template.approx != "SEOBNRv3"
        ):  # LAL non-higher mode waveforms
            modes_NR = {}
            for ell in range(2, ell_max + 1):
                for m in range(-ell, ell + 1):
                    modes_NR[ell, m] = lalsim.SphHarmTimeSeriesGetMode(
                        hlm_NR, ell, m
                    ).data.data
            tmp = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2)
            time_NR = tmp.deltaT * np.arange(len(tmp.data.data))
            NR_duration = get_time_to_merger(time_NR, modes_NR)
            NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI

        else:

            modes_NR = {}
            for ell in range(2, ell_max + 1):
                for m in range(-ell, ell + 1):
                    modes_NR[ell, m] = lalsim.SphHarmTimeSeriesGetMode(
                        hlm_NR, ell, m
                    ).data.data
            tmp = lalsim.SphHarmTimeSeriesGetMode(hlm_NR, 2, 2)
            time_NR = tmp.deltaT * np.arange(len(tmp.data.data))
            NR_duration = get_time_to_merger(time_NR, modes_NR)
            NR_duration /= (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI

    # print("NR_duration = ", NR_duration,", fmin*0.95 = ", 0.95 * params_signal.f_min, " , fmin*1.05 =", 1.05 * params_signal.f_min)
    # print("params_template = ", params_template)
    # print("ell_max = ", ell_max)
    print("Before calling root_scalar")
    try:
        res = root_scalar(
            tdiff,
            bracket=(0.95 * params_signal.f_min, 1.05 * params_signal.f_min),
            args=(NR_duration, params_template, ell_max),
        )
        result = res.root
    except ValueError:
        result = lalsim.EOBHighestInitialFreq(params_signal.m1 + params_signal.m2)

    return result


def get_final_modes(
    f_min_EOB: float,
    params: waveform_params,
    case: str = None,
    ell_max: int = 4,
    write_file: bool = False,
) -> Tuple[np.array, np.array]:
    """Save the EOB modes to file given the minimum EOB frequency

    Args:
        f_min_EOB (float): The new minimum EOB frequency
        params (waveform_params): The waveform parameters
        case (str): Name for the directory where to save the data
    """
    m1 = params.m1
    m2 = params.m2
    if params.approx == "SEOBNRv4PHM":
        ell_max = 1 * ell_max
    elif params.approx == "SEOBNRv4P":
        ell_max = 2
    elif params.approx == "SEOBNRv3":
        ell_max = 2
    amp_factor = (m1 + m2) * lal.MRSUN_SI / params.distance
    params.f_min = f_min_EOB
    # print("Generating final EOB modes with ell_max={}".format(ell_max))
    hlm_EOB = generate_EOB_modes(params, ell_max)
    tmp = lalsim.SphHarmTimeSeriesGetMode(hlm_EOB, 2, 2)
    time_EOB = tmp.deltaT * np.arange(len(tmp.data.data)) / ((m1 + m2) * lal.MTSUN_SI)
    modes_EOB = {}
    for ell in range(2, ell_max + 1):
        for m in range(-ell, ell + 1):
            modes_EOB["{},{}".format(ell, m)] = (
                1
                / amp_factor
                * lalsim.SphHarmTimeSeriesGetMode(hlm_EOB, ell, m).data.data
            )
    if case and write_file:
        try:
            os.mkdir(case)
        except:
            pass

        write_rhOverM_h5_SEOBNR(time_EOB, modes_EOB, case, ell_max)
    return time_EOB, modes_EOB


def generate_EOB_modes(params: waveform_params, ell_max: int = 4):
    """Generate SEOBNRv4PHM modes

    Args:
        params (waveform_params): The parameters to use
        ell_max (int, optional): The maximum ell to use. Defaults to 4.

    Returns:
        [type]: The modes
    """
    if params.approx == "SEOBNRv4PHM" or params.approx == "SEOBNRv4P":
        params_EOB = lal.CreateDict()
        ma = lalsim.SimInspiralCreateModeArray()
        lalsim.SimInspiralModeArrayActivateMode(ma, 2, 2)
        lalsim.SimInspiralModeArrayActivateMode(ma, 2, 1)
        if ell_max > 2:
            lalsim.SimInspiralModeArrayActivateMode(ma, 3, 3)
        if ell_max > 3:
            lalsim.SimInspiralModeArrayActivateMode(ma, 4, 4)
        if ell_max > 4:
            lalsim.SimInspiralModeArrayActivateMode(ma, 5, 5)

        lalsim.SimInspiralWaveformParamsInsertModeArray(params_EOB, ma)
        hlm = lalsim.SimInspiralChooseTDModes(
            0.0,
            params.delta_t,
            params.m1 * lal.MSUN_SI,
            params.m2 * lal.MSUN_SI,
            params.s1x,
            params.s1y,
            params.s1z,
            params.s2x,
            params.s2y,
            params.s2z,
            params.f_min,
            params.f_ref,
            params.distance,
            params_EOB,
            ell_max,
            lalsim.GetApproximantFromString(params.approx),
        )
    else:
        hlm = lalsim.SimInspiralChooseTDModes(
            0.0,
            params.delta_t,
            params.m1 * lal.MSUN_SI,
            params.m2 * lal.MSUN_SI,
            params.s1x,
            params.s1y,
            params.s1z,
            params.s2x,
            params.s2y,
            params.s2z,
            params.f_min,
            params.f_ref,
            params.distance,
            None,
            ell_max,
            lalsim.GetApproximantFromString(params.approx),
        )

    return hlm


# Adding if to identify SEOBNREv4 and TEOBResumSe
def get_approximant_type(approx: str) -> str:
    """Return whether the approximant is precessing or not

    Args:
        approx (str): The name of the approximant'

    Returns:
        str: "aligned" or "precessing"
    """
    if approx == "SEOBNREv4" or approx == "TEOBResumSe" or approx == "TEOBResumSeHM":
        return "aligned"
    else:
        approx_enum = lalsim.GetApproximantFromString(approx)
        status = lalsim.SimInspiralGetSpinFreqFromApproximant(approx_enum)
        if status == lalsim.SIM_INSPIRAL_SPINS_NONPRECESSING:
            return "aligned"
        else:
            return "precessing"


def write_serialized_output(
    fname,
    total_masses,
    unfaithfulness,
    SNR,
    phases,
    time_shifts,
    polarizations,
    **kwargs,
):
    """Write extensive serialized output which allows one to reconstuct the best matching waveform should it be
    desired

    Args:
        fname (str): the name of the file to write
        total_masses (array): array of total masses
        unfaithfulness (array): unfaithfulness
        SNR (array): matched filter SNR
        phases (array): the phase giving best overlap
        time_shifts (array): timeshift giving the best overlap
        polarizations (array): polarization shift giving the best overlap
    """
    fp = h5py.File(fname, "w")
    df = pd.DataFrame()
    pd["Mtot"] = total_masses
    pd["unfaitfulness"] = unfaithfulness
    pd["SNR"] = SNR
    pd["phases"] = phases
    pd["time_shifts"] = time_shifts
    pd["polarizations"] = polarizations
    keys = kwargs.keys()
    if "alphas" in keys:
        pd["alphas"] = kwargs["alphas"]
    elif "f_refs" in keys:
        pd["f_refs"] = kwargs["f_refs"]
    if "metadata" in keys:
        for key in metadata.keys():
            fp.attrs[key] = metadata[key]

    fp.close()
