#!/usr/bin/env python
import argparse
import glob
import os

import numpy as np

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--case-file", type=str, help="File with the cases")
    args = p.parse_args()
    if args.case_file:
        with open(args.case_file, "r") as fp:
            cases = fp.readlines()
    else:
        cases = set([x[:-17] for x in glob.glob("*result*")])
        print(len(cases))
    for case in cases:
        if "." in case:
            name = case.split(".")[0]
        else:
            name = case
        if os.path.isfile("mismatches_{}.dat".format(name)):
            print("Skipping {}".format(case))
            continue
        print(case, name)
        lst = glob.glob("{}*".format(name))
        if not lst:
            continue
        weighted_matches = 0.0
        SNRs = 0.0
        mismatches = 0.0

        # Note that the code returns *mismatches* so when averaging over SNR
        # we consider (1-mm) and average the _match_ and then subtract the
        # average from 1 at the end. See Eq(11) in https://arxiv.org/pdf/1809.10113.pdf
        for it in lst:
            d = np.genfromtxt(it)
            print(d.shape)
            if len(d.shape) > 1:
                weighted_matches += (1 - d[:, 1]) ** 3 * d[:, -1] ** 3
                mismatches += d[:, 1]
                SNRs += d[:, -1] ** 3
                masses = d[:, 0]
            else:
                weighted_matches += (1 - d[1]) ** 3 * d[-1] ** 3
                mismatches += d[1]
                SNRs += d[-1] ** 3
                masses = d[0]
        mismatches /= len(lst)

        lst = glob.glob("res*.dat")
        averaged_mismatches = 1 - (weighted_matches / SNRs) ** (1.0 / 3)

        res = np.c_[masses, mismatches, averaged_mismatches]
        np.savetxt("mismatches_{}.dat".format(name), res)
