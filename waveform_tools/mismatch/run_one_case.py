#!/usr/bin/env/python3
import argparse
import numpy as np
import sys

sys.path.append("/home/sossokine/Sources/waveform_tools")
from waveform_tools.mismatch.unfaithfulness import run_unfaithfulness
from joblib import delayed, Parallel


def main():
    p = argparse.ArgumentParser()
    p.add_argument("--NR_file", type=str, help="The NR file")
    p.add_argument(
        "--signal_approximant",
        type=str,
        help="What type of NR file do we have?",
        default="NR_hdf5",
    )
    p.add_argument("--approximant", type=str, help="Approximant to use for template")
    p.add_argument("--ell_max", type=int, help="Maximum ell to include", default=4)
    p.add_argument(
        "--flow",
        type=float,
        help="The lower bound of unfaithfulness calculation",
        default=10.0,
    )
    p.add_argument(
        "--fudge_factor",
        type=float,
        help="The factor to multiply the lowest NR frequency by when computing unfaithfulness integration bounds",
        default=1.05,
    )
    p.add_argument(
        "--mass_range",
        type=str,
        help="The mass range to use for unfaithfulness computation. Should have the form mlow,mhigh,mstep",
        default="20,300,20",
    )

    p.add_argument(
        "--iota_s", type=float, help="The inclination of the source", default=np.pi / 3
    )
    p.add_argument(
        "--min_type",
        type=str,
        help="For Phenom waveforms whether to minimize over reference frequency or rigid rotation of the spins",
        default="reference_frequency",
    )
    p.add_argument(
        "--unfaithfulness_type",
        type=str,
        help="The type of unfaithfulness to use.",
        default="unfaithfulness_RC",
    )

    args = p.parse_args()

    phis = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    kappas = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    masses = tuple(float(x) for x in args.mass_range.split(","))
    x, y = np.meshgrid(kappas, phis)
    x = x.flatten()
    y = y.flatten()

    Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(run_unfaithfulness)(
            y[i],
            x[i],
            args.iota_s,
            i,
            NR_file=args.NR_file,
            ellMax=args.ell_max,
            template_approx=args.approximant,
            signal_approx=args.signal_approximant,
            fhigh=2048.0,
            flow_d=args.flow,
            minimization_type=args.min_type,
            unfaithfulness_type=args.unfaithfulness_type,
            fudge_factor=args.fudge_factor,
            mass_range=masses,
        )
        for i in range(len(x))
    )


if __name__ == "__main__":
    main()
