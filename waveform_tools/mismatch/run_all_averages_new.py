#!/usr/bin/env python
import argparse
import glob
import os
import subprocess as sp
import uuid

# Create a function called "chunks" with two arguments, l and n:
def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i : i + n]


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument(
        "--run_dir",
        type=str,
        help="Directory where the results will be output",
        default=os.getcwd(),
    )
    p.add_argument("--file_list", type=str, help="List of all the files to run on")
    p.add_argument(
        "--approximant", type=str, help="Approximant to use", default="IMRPhenomPv3HM"
    )
    p.add_argument("--ell_max", type=int, help="Maximum ell to use", default=4)
    p.add_argument(
        "--flow",
        type=float,
        help="The lower bound of unfaithfulness calculation",
        default=10.0,
    )
    p.add_argument(
        "--fudge_factor",
        type=float,
        help="The factor to multiply the lowest NR frequency by when computing unfaithfulness integration bounds",
        default=1.05,
    )
    p.add_argument(
        "--mass_range",
        type=str,
        help="The mass range to use for unfaithfulness computation. Should have the form mlow,mhigh,mstep",
        default="20,300,20",
    )
    p.add_argument(
        "--min_type",
        type=str,
        help="Whether to minimize over reference  frequency or spin rotation. Applies only to Phenom waveforms",
        default="reference_frequency",
    )
    p.add_argument(
        "--unfaithfulness_type",
        type=str,
        help="The type of unfaithfulness to use.",
        default="unfaithfulness_RC",
    )
    p.add_argument("--N", type=int, help="The number of cases in a chunk", default=8)
    p.add_argument(
        "--submit", action="store_true", help="Submit the jobs that have been created"
    )
    args = p.parse_args()

    run_dir = os.path.abspath(args.run_dir)
    try:
        os.chdir(run_dir)
    except OSError:
        print("Could not go into the run directory {}, quitting!".format(run_dir))
        exit(-1)

    script_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    with open(args.file_list, "r") as fp:
        lst = fp.readlines()
    chk = list(chunks(lst, args.N))
    header = """#!/bin/bash -                                                                                      
#SBATCH -J chunk_{}                 # Job Name                                                                         
#SBATCH -o chunk_{}.stdout          # Output file name                                                
#SBATCH -e chunk_{}.stderr          # Error file name                                               
#SBATCH -n 16                 # Number of cores                                         
#SBATCH --ntasks-per-node 16  # number of MPI ranks per node                                  
#SBATCH -p nr                 # Queue name                                                               
#SBATCH -t 18:0:00           # Run time                                                                     
#SBATCH --no-requeue                                                                                                                   
source {}/bin/activate

cd {}
"""
    unq_name = uuid.uuid4().hex
    root = "/work/sossokine/LVCFormatWaveforms"
    for i in range(len(chk)):
        nm = unq_name + "_{}".format(i)
        fp = open("chunk_{}.sh".format(nm), "w")
        fp.write(header.format(nm, nm, nm, os.environ["ENV_HOME"], run_dir))
        for it in chk[i]:
            NR_file = "{}/{}".format(root, it.strip())
            cmd = """python {}/run_one_case.py --NR_file {} --approximant {} --ell_max {} --min_type {} --unfaithfulness_type {} --flow {} --fudge_factor {} --mass_range {}\n""".format(
                script_dir,
                NR_file,
                args.approximant,
                args.ell_max,
                args.min_type,
                args.unfaithfulness_type,
                args.flow,
                args.fudge_factor,
                args.mass_range,
            )
            fp.write(cmd)
        fp.close()
        submit_cmd = "sbatch chunk_{}.sh".format(nm)
        print(submit_cmd)
        if args.submit:
            # print(args.submit)
            sp.call(submit_cmd, shell=True)
        else:
            print(
                ".sh files created. To also automatically submit, rerun with --submit"
            )
