#!/usr/bin/env python3
import argparse
import numpy as np
import pandas as pd
from waveform_tools.mismatch.unfaithfulness import run_unfaithfulness
from joblib import Parallel, delayed


def compare_waveforms(
    phi_s: float,
    kappa_s: float,
    iota_s: float,
    NR_file: str,
    NR_file_2: str,
    index: int,
    ell_max: int = 4,
):
    signal_approx = "NR_custom"
    template_approx = "NR_custom"
    run_unfaithfulness(
        phi_s,
        kappa_s,
        iota_s,
        index,
        template_approx=template_approx,
        signal_approx=signal_approx,
        NR_file=NR_file,
        NR_file_2=NR_file_2,
        ellMax=ell_max,
    )


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--NR_file", type=str, help="The first NR file")
    p.add_argument("--NR_file_2", type=str, help="The second NR file")
    p.add_argument(
        "--iota_s",
        type=float,
        help="The inclination angle of the source (rad)",
        default=np.pi / 3,
    )
    p.add_argument("--ell_max", type=int, help="The max ell to use", default=4)

    args = p.parse_args()
    phis = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    kappas = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    x, y = np.meshgrid(kappas, phis)
    x = x.flatten()
    y = y.flatten()
    Parallel(n_jobs=2, verbose=50, backend="multiprocessing")(
        delayed(compare_waveforms)(
            y[i],
            x[i],
            args.iota_s,
            args.NR_file,
            args.NR_file_2,
            i,
            ell_max=args.ell_max,
        )
        for i in range(len(x))
    )
