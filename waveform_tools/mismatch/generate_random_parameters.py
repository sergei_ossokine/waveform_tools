#!/usr/bin/env python3
import argparse
import numpy as np
import pandas as pd

def generate_random_params(
            q_min: float,
            q_max: float,
            chi_min: float,
            chi_max: float,
            feducial_freq: float = 1200,
            N: int = 10,
        ):
    """Generate a set of random parameters                                                                                                                                                                                                                                    
        
    Args:                                                                                                                                                                                                                                                                     
    q_min (float): Minimum mass ratio                                                                                                                                                                                                                                     
    q_max (float): Maximum mass ratio                                                                                                                                                                                                                                     
    chi_min (float): Minimum dimensionless spin                                                                                                                                                                                                                           
    chi_max (float): Maximum dimensionless spin                                                                                                                                                                                                                           
    feducial_freq (float, optional): The frequency to start waveform generation. In Hz, at total mass of 1 solar mass. Default to 1200                                                                                                                                    
    N (int, optional): Number of cases to generate. Defaults to 10.                                                                                                                                                                                                       
    
    Returns:                                                                                                                                                                                                                                                                  
    np.array: An array of cases                                                                                                                                                                                                                                           
    """
    q = np.random.uniform(q_min, q_max, size=N)
    chi1_mag = np.random.uniform(chi_min, chi_max, size=N)
    chi2_mag = np.random.uniform(chi_min, chi_max, size=N)
    
    cs_theta1 = np.random.uniform(-1, 1, size=N)
    theta1 = np.arccos(cs_theta1)
    cs_theta2 = np.random.uniform(-1, 1, size=N)
    theta2 = np.arccos(cs_theta2)
    
    phi1 = np.random.uniform(0, 2 * np.pi, size=N)
    phi2 = np.random.uniform(0, 2 * np.pi, size=N)
    chi1 = (
        chi1_mag[:, None]
        * np.c_[
            np.sin(theta1) * np.cos(phi1), np.sin(theta1) * np.sin(phi1), np.cos(theta1)
        ]
    )
    chi2 = (
        chi2_mag[:, None]
        * np.c_[
            np.sin(theta2) * np.cos(phi2), np.sin(theta2) * np.sin(phi2), np.cos(theta2)
        ]
    )
    freq_1M = np.ones(N) * feducial_freq
    data = np.c_[q, chi1, chi2, freq_1M]
    header = ["q", "chi1x","chi1y","chi1z","chi2x","chi2y","chi2z", "freq_1M"]
    df = pd.DataFrame(data=data, columns=header)
    df.to_csv("random_params.csv",index=False)


if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--chi_min",type=float,help="Minimum magnitude of spins")
    p.add_argument("--chi_max",type=float,help="The maximum magnitude of spins")
    p.add_argument("--q_min",type=float,help="The minimum mass ratio, >=1")
    p.add_argument("--q_max",type=float,help="The maximum mass ratio, >=1")
    p.add_argument("--feducial_freq",type=float,help="Frequency in Hz for a 1 solar mass binary",default=200)
    p.add_argument("--N",type=int,help="Number of configurations",default=1000)
    
    args = p.parse_args()
    generate_random_params(args.q_min,args.q_max,args.chi_min,args.chi_max,args.feducial_freq,args.N)
    
