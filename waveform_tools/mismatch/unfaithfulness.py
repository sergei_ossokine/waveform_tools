import argparse
import os
import re
from copy import deepcopy
from functools import lru_cache
from typing import Dict, Union

import h5py
import lal
import lalsimulation as lalsim
import loguru
import numpy as np
import pandas as pd
import pycbc.filter as _filter
import pycbc.types as pt
import pycbc.waveform as pw
from joblib import Parallel, delayed
from pycbc.filter import make_frequency_series
from pycbc.filter.matchedfilter import match
from pycbc.psd.analytical import aLIGOZeroDetHighPower, aLIGOZeroDetHighPowerGWINC
from pycbc.types import TimeSeries
from pycbc.types.frequencyseries import FrequencySeries
from pycbc.waveform import fd_approximants, td_approximants
from pycbc.waveform.utils import taper_timeseries
from scipy import optimize
from scipy.interpolate import InterpolatedUnivariateSpline
from scipy.optimize import brute, dual_annealing, minimize, minimize_scalar
from waveform_tools.mismatch.auxillary_funcs import *
from waveform_tools.mismatch.eccentric_waveforms import *
from waveform_tools.mismatch.waveform_parameters import waveform_params

EOB_approximants = ["SEOBNRv3", "SEOBNRv4P", "SEOBNRv4PHM", "SEOBNRv4PHMSur"]
eEOB_approximants = ["TEOBResumSe", "TEOBResumSe", "SEOBNREv4"]


import sxs

# SEOBNRv5_wrapper = f"{os.environ['SEOBNRv5_wrapper']}/calibration"
# sys.path.append(SEOBNRv5_wrapper)
# print(sys.path)
# from EOB_v5_wrapper import get_EOB_modes
# from TEOB_wrapper import get_EOB_modes_TEOB


def get_NR_data(NR_file: str, ell_max: int = 4, format="SpEC"):
    # This is a custom NR approximant, needed for NR-NR match
    # and symmetrized NR
    wf_modes = {}
    fp = h5py.File(NR_file, "r")
    if format == "AEI":
        tr = fp["tr"].value
        for ell in range(2, ell_max + 1):
            for m in range(-ell, ell + 1):
                key = "Yl_{}_{}".format(ell, m)
                d = fp[key].value
                t = d[:, 0]
                idx = np.where(t >= tr)

                wf_modes["{},{}".format(ell, m)] = d[:, 1][idx] + 1j * d[:, 2][idx]
        chi1 = fp["chiA_tr"].value
        chi2 = fp["chiB_tr"].value
        m1_dim = fp["mA_tr"].value
        m2_dim = fp["mB_tr"].value
        omega_start = fp["omega_mag_tr"].value
    elif format == "SpEC":
        # Get metadata
        dirname = os.path.dirname(NR_file)
        metaname = f"{dirname}/metadata.txt"
        metadata = sxs.Metadata.from_txt_file(metaname)
        m1_dim = metadata["reference_mass1"]
        m2_dim = metadata["reference_mass2"]
        chi1 = metadata["reference_dimensionless_spin1"]
        chi2 = metadata["reference_dimensionless_spin2"]
        omega_start = np.linalg.norm(metadata["reference_orbital_frequency"])
        tr = metadata["reference_time"]
        for ell in range(2, ell_max + 1):
            for m in range(-ell, ell + 1):
                key = "Extrapolated_N2.dir/Y_l{}_m{}.dat".format(ell, m)
                d = fp[key].value
                t = d[:, 0]
                idx = np.where(t >= tr)

                wf_modes["{},{}".format(ell, m)] = d[:, 1][idx] + 1j * d[:, 2][idx]
    fp.close()
    freq_1M = omega_start / (m1_dim + m2_dim) / (lal.MTSUN_SI) / np.pi
    params = dict(m1_dim=m1_dim, m2_dim=m2_dim, chi1=chi1, chi2=chi2, freq_1M=freq_1M)
    return wf_modes, t[idx] - tr, params


@lru_cache(maxsize=128)
def generate_psd(
    length: int, delta_f: float, flow: float, psd_type: str = "aLIGO"
) -> pt.FrequencySeries:
    """A memoized version of aLIGOZeroDetHighPowerGWINC

    Args:
        length (int): length of the PSD to generate
        delta_f (float): frequency spacing
        flow (float): low frequency cutoff

    Returns:
        pt.FrequencySeries: the PSD
    """
    if psd_type == "aLIGO":
        return aLIGOZeroDetHighPowerGWINC(length, delta_f, flow)
    elif psd_type == "flat":
        return FrequencySeries(np.ones(length), delta_f=delta_f)
    else:
        raise NotImplementedError


def generate_waveform(
    p: waveform_params, f_max: float = 2048.0
) -> Union[pt.TimeSeries, pt.FrequencySeries]:
    """Generate a lalsuite waveform. Explicitly uses
    which domain is selected, either TD or FD.

    Args:
        p (waveform_params): The parameters of the waveform
        f_max (float, optional): The upper frequency cutoff for the waveform. Defaults to 2048.0.

    Raises:
        NotImplementedError: If you try a domain other than "TD" or "FD"

    Returns:
        Union[pt.TimeSeries, pt.FrequencySeries]: The desired waveform
    """
    # print("domain = ",p.domain)
    # print(p)
    if p.domain == "TD":
        # if p.approx == "TEOBResumSe" or p.approx == "SEOBNREv4":

        if p.approx == "TEOBResumSe":
            modesTEOB = [[2, 2]]
            t, hp, hc, hlm = generate_TEOBResumSe_simple_waveform(p, f_max, modesTEOB)

        elif p.approx == "TEOBResumSeHM":
            modesTEOB = [
                [2, 2],
                [2, 1],
                [3, 3],
                [3, 2],
                [4, 4],
                [5, 5],
            ]  # More reliable modes
            t, hp, hc, hlm = generate_TEOBResumSe_simple_waveform(p, f_max, modesTEOB)

        elif p.approx == "SEOBNREv4":
            t, hp, hc, amp22 = generate_SEOBNREv4_waveform(p, f_max)

        # Read output file of the SEOBNREv3 output file
        else:
            try:
                hp, hc = lalsim.SimInspiralChooseTDWaveform(
                    p.m1 * lal.MSUN_SI,
                    p.m2 * lal.MSUN_SI,
                    p.s1x,
                    p.s1y,
                    p.s1z,
                    p.s2x,
                    p.s2y,
                    p.s2z,
                    p.distance,
                    p.iota,
                    p.phi,
                    0.0,
                    0.0,
                    0.0,
                    p.delta_t,
                    p.f_min,
                    p.f_ref,
                    p.wf_param,
                    lalsim.GetApproximantFromString(p.approx),
                )
            except:
                m1 = p.m1 * (1 + 1e-8)
                m2 = p.m2 * (1 + 1e-8)
                hp, hc = lalsim.SimInspiralChooseTDWaveform(
                    m1 * lal.MSUN_SI,
                    m2 * lal.MSUN_SI,
                    p.s1x,
                    p.s1y,
                    p.s1z,
                    p.s2x,
                    p.s2y,
                    p.s2z,
                    p.distance,
                    p.iota,
                    p.phi,
                    0.0,
                    0.0,
                    0.0,
                    p.delta_t,
                    p.f_min,
                    p.f_ref,
                    p.wf_param,
                    lalsim.GetApproximantFromString(p.approx),
                )

        if (
            p.approx == "SEOBNREv4"
            or p.approx == "TEOBResumSe"
            or p.approx == "TEOBResumSeHM"
        ):
            # Taper
            hp_td = TimeSeries(hp, delta_t=p.delta_t)
            hc_td = TimeSeries(hc, delta_t=p.delta_t)
            hp_td = taper_timeseries(hp_td, tapermethod="startend")
            hc_td = taper_timeseries(hc_td, tapermethod="startend")
        else:
            hp_td = TimeSeries(hp.data.data, delta_t=p.delta_t)
            hc_td = TimeSeries(hc.data.data, delta_t=p.delta_t)
            # Taper
            # The 35 means that the roll-off starts 35 M before the end of the signal
            # in geometric units
            hp_td = taper_pycbc_series(
                hp_td, ending_time=35 * (p.m1 + p.m2) * lal.MTSUN_SI
            )
            hc_td = taper_pycbc_series(
                hc_td, ending_time=35 * (p.m1 + p.m2) * lal.MTSUN_SI
            )
            # hp_td = taper_timeseries(hp_td, tapermethod="startend")
            # hc_td = taper_timeseries(hc_td, tapermethod="startend")

        return hp_td, hc_td

    elif p.domain == "FD":

        hp, hc = lalsim.SimInspiralChooseFDWaveform(
            p.m1 * lal.MSUN_SI,
            p.m2 * lal.MSUN_SI,
            p.s1x,
            p.s1y,
            p.s1z,
            p.s2x,
            p.s2y,
            p.s2z,
            p.distance,
            p.iota,
            p.phi,
            0.0,
            0.0,
            0.0,
            p.delta_f,
            p.f_min,
            f_max,
            p.f_ref,
            p.wf_param,
            lalsim.GetApproximantFromString(p.approx),
        )
        hp, hc = lalsim.SimInspiralChooseFDWaveform(
            m1=p.m1 * lal.MSUN_SI,
            m2=p.m2 * lal.MSUN_SI,
            S1x=p.s1x,
            S1y=p.s1y,
            S1z=p.s1z,
            S2x=p.s2x,
            S2y=p.s2y,
            S2z=p.s2z,
            distance=p.distance,
            inclination=p.iota,
            LALpars=p.wf_param,
            phiRef=p.phi,
            f_ref=p.f_ref,
            deltaF=p.delta_f,
            f_min=p.f_min,
            f_max=f_max,
            longAscNodes=np.pi / 2.0,
            eccentricity=0.0,
            meanPerAno=0.0,
            approximant=lalsim.GetApproximantFromString(p.approx),
        )
        hp_tilde = pt.FrequencySeries(hp.data.data, p.delta_f)
        hc_tilde = pt.FrequencySeries(hc.data.data, p.delta_f)
        return hp_tilde, hc_tilde

    else:
        # Should never get here
        raise NotImplementedError


def minimize_unfaithfulness(
    min_pars: np.array,
    s: Union[pt.TimeSeries, pt.FrequencySeries],
    params_template: waveform_params,
    kappa_s: float,
    flow: float,
    fhigh: float = None,
    quantity: str = "unfaithfulness_RC",
    modes_dc: Dict = None,
    debug: bool = False,
    min_type: str = "phi",
) -> float:
    """Function called to minimize the unfaithfulness numerically over the phase and eccentricity and l0.
    The quantity to be minimized can currently be:
    1. 'unfaithfulness_RC' as defined in Cotesta et al (https://arxiv.org/pdf/1803.10701.pdf)
    2. 'unfaithfulness_IH' as defined in Harry et al(https://arxiv.org/pdf/1603.02444.pdf)


    Args:
        min_pars (np.array): the minimization parameters, f_ref,phi
        params_signal (waveform_params): Parameters of the signal waveform
        params_template (waveform_params): Parameters of the template waveform
        kappa_s (float): The effective polarization of the signal
        flow (float): The lower frequency bound
        fhigh (float, optional): The higher frequency bound. Defaults to None.
        quantity (str, optional): The quantity to minimize. Can either be 'unfaithfulness_RC' or 'unfaithfulness_IH'
        modes_dc (dict,optional): A dictionary of waveform modes. Must already have the right time spacing
    Returns:
        float: the unfaithfulness
    """

    ## This is for QC binaries
    if min_type == "phi":

        phi_t = min_pars
        if isinstance(phi_t, (list, np.ndarray)):
            phi_t = phi_t[0]
        params_template.phi = phi_t

    elif min_type == "reference_frequency":  # For precessing Phenom models

        f_ref_t, phi_t = min_pars
        if f_ref_t < 0.8 * params_template.f_min:
            f_ref_t = 0.8 * params_template.f_min

        # Needed for IMRPhenomTPHM
        if params_template.f_min > f_ref_t:
            params_template.f_min = f_ref_t

        params_template.f_ref = f_ref_t
        params_template.phi = phi_t

    elif min_type == "spin_rotation":  # For precessing Phenom models

        alpha, phi_t = min_pars
        # Rotate the spins  counter-clockwise around the z-axis by alpha
        params_template.phi = phi_t
        params_template.s1x, params_template.s1y = rotate_inplane_spin(
            params_template_f.s1x, params_template_f.s1y, alpha
        )
        params_template.s2x, params_template.s2y = rotate_inplane_spin(
            params_template_f.s2x, params_template_f.s2y, alpha
        )

    elif min_type == "eccentricity_22mode":
        # We are maximizing over phase and initial eccentricity
        ecc_t = min_pars
        # print(" phi_t = ",phi_t, ",    ecc_t  = ",ecc_t)
        params_template.ecc = ecc_t

    ## Now for eccentric models . Optimize numerically also over eccentricity or mean anomaly
    elif min_type == "eccentricity":
        # We are maximizing over phase and initial eccentricity
        phi_t, ecc_t = min_pars
        # print(" phi_t = ",phi_t, ",    ecc_t  = ",ecc_t)
        params_template.ecc = ecc_t
        params_template.phi = phi_t

    elif min_type == "eccentricity_meanAnomaly":
        # We are maximizing over phase, initial eccentricity and meanAnomaly
        phi_t, ecc_t, l0_t = min_pars

        params_template.ecc = ecc_t
        params_template.phi = phi_t
        params_template.mean_anomaly = l0_t

    elif min_type == "eccentricity_reference_frequency":
        # We are maximizing over reference frequency, phase and initial eccentricity
        phi_t, ecc_t, f_ref_t = min_pars

        if f_ref_t < 0.8 * params_template.f_min:
            f_ref_t = 0.8 * params_template.f_min

        params_template.f_ref = f_ref_t
        params_template.phi = phi_t
        params_template.ecc = ecc_t

    elif min_type == "eccentricity_spin_rotation":

        alpha, ecc_t, phi_t = min_pars
        # Rotate the spins  counter-clockwise around the z-axis by alpha
        params_template.phi = phi_t
        params_template.ecc = ecc_t
        params_template.s1x, params_template.s1y = rotate_inplane_spin(
            params_template_f.s1x, params_template_f.s1y, alpha
        )
        params_template.s2x, params_template.s2y = rotate_inplane_spin(
            params_template_f.s2x, params_template_f.s2y, alpha
        )

    elif min_type == "eccentricity_mean_anomaly_reference_frequency":

        phi_t, ecc_t, l0_t, f_ref_t = min_pars
        # Rotate the spins  counter-clockwise around the z-axis by alpha
        params_template.phi = phi_t
        params_template.ecc = ecc_t
        params_template.mean_anomaly = l0_t

        if f_ref_t < 0.8 * params_template.f_min:
            f_ref_t = 0.8 * params_template.f_min

        params_template.f_ref = f_ref_t

    elif min_type == "eccentricity_mean_anomaly_spin_rotation":

        alpha, ecc_t, phi_t, l0_t = min_pars
        # Rotate the spins  counter-clockwise around the z-axis by alpha
        params_template.phi = phi_t
        params_template.ecc = ecc_t
        params_template.mean_anomaly = l0_t

        params_template.s1x, params_template.s1y = rotate_inplane_spin(
            params_template_f.s1x, params_template_f.s1y, alpha
        )
        params_template.s2x, params_template.s2y = rotate_inplane_spin(
            params_template_f.s2x, params_template_f.s2y, alpha
        )

    else:
        # We are maximizing over phase only
        phi_t = min_pars[0]
        params_template.phi = phi_t

    # print(" phi_t = ", params_template.phi)
    if quantity == "unfaithfulness_RC":
        # print(f"f_low={flow}")
        return unfaithfulness_RC(
            s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug
        )
    elif quantity == "unfaithfulness_IH":
        return unfaithfulness_IH(
            s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc
        )
    else:
        raise NotImplementedError


def rotate_inplane_spin(s1x: float, s1y: float, alpha: float):
    """Perform a rigid rotation of the in-plane components of the spin
    by angle alpha, counterclockwise around the z-axis

    Args:
        s1x (float): x-component
        s1y (float): y-component
        alpha (float): angle in radians
    """
    sx_new = np.cos(alpha) * s1x - np.sin(alpha) * s1y
    sy_new = np.sin(alpha) * s1x + np.cos(alpha) * s1y
    return sx_new, sy_new


# With the new minimize_unfaithfulness function, this function below is no longer needed
# Left here for legacy
def minimize_unfaithfulness_spin_rotation(
    min_pars: np.array,
    s: Union[pt.TimeSeries, pt.FrequencySeries],
    params_template_f: waveform_params,
    kappa_s: float,
    flow: float,
    fhigh: float = None,
    quantity: str = "unfaithfulness_RC",
    modes_dc: Dict = None,
    debug: bool = False,
) -> float:
    """Function called to minimize the unfaithfulness numerically over the phase and f_ref.
    The quantity to be minimized can currently be:
    1. 'unfaithfulness_RC' as defined in Cotesta et al (https://arxiv.org/pdf/1803.10701.pdf)
    2. 'unfaithfulness_IH' as defined in Harry et al(https://arxiv.org/pdf/1603.02444.pdf)


    Args:
        min_pars (np.array): the minimization parameters, f_ref,alpha
        params_template (waveform_params): Parameters of the template waveform
        kappa_s (float): The effective polarization of the signal
        flow (float): The lower frequency bound
        fhigh (float, optional): The higher frequency bound. Defaults to None.
        quantity (str, optional): The quantity to minimize. Can either be 'unfaithfulness_RC' or 'unfaithfulness_IH'
        modes_dc (dict,optional): A dictionary of waveform modes. Must already have the right time spacing
    Returns:
        float: the unfaithfulness
    """
    params_template = deepcopy(params_template_f)
    # Update the template parameters
    if len(min_pars) > 1:
        # We are maximizing over rigid rotation of the spin and phase
        alpha, phi_t = min_pars
        # Rotate the spins  counter-clockwise around the z-axis by alpha
        params_template.phi = phi_t
        params_template.s1x, params_template.s1y = rotate_inplane_spin(
            params_template_f.s1x, params_template_f.s1y, alpha
        )
        params_template.s2x, params_template.s2y = rotate_inplane_spin(
            params_template_f.s2x, params_template_f.s2y, alpha
        )

        # print(alpha, params_template.phi)
    else:
        # We are maximizing over phase only
        phi_t = min_pars[0]
        params_template.phi = phi_t

    if quantity == "unfaithfulness_RC":
        return unfaithfulness_RC(
            s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc, debug=debug
        )
    elif quantity == "unfaithfulness_IH":
        return unfaithfulness_IH(
            s, params_template, flow, fhigh=fhigh, modes_dict=modes_dc
        )
    else:
        raise NotImplementedError


def unfaithfulness_RC(
    s_arg: Union[pt.TimeSeries, pt.FrequencySeries],
    params_template: waveform_params,
    flow: float = 10.0,
    fhigh: float = None,
    modes_dict: Dict = None,
    debug: bool = False,
    psd_type="aLIGO",
) -> Union[float, tuple]:
    """This function returns the overlap maximized over the effective polarization
    for given value of the phi.
    See https://arxiv.org/pdf/1803.10701.pdf and https://arxiv.org/pdf/1709.09181.pdf

    Args:
        s_arg (pt.TimeSeries): The signal in the detector
        params_template (waveform_params): The template parameters
        flow (float, optional): Low frequency cutoff. Defaults to 10.0.
        fhigh (float, optional): High frequency cutoff. Defaults to None.
        modes_dict (Dict, optional): Dictionary of modes. To be used for EOB. Defaults to None.
        debug (bool, optional): Outout debug information. Defaults to False.

    Returns:
        float: The unfaithfulness
    """
    s = s_arg.copy()
    if params_template.domain == "TD":
        # If we are not given a dictionary of modes, generate the template from scratch
        if not modes_dict:
            hp_td, hc_td = generate_waveform(params_template)
        else:
            # Combine modes
            hp_t, hc_t = combine_modes(
                params_template.iota, params_template.phi, modes_dict
            )

            # Taper
            hp_td = TimeSeries(hp_t, delta_t=params_template.delta_t)
            hc_td = TimeSeries(hc_t, delta_t=params_template.delta_t)
            # hp_td = taper_timeseries(hp_td, tapermethod="startend")
            # hc_td = taper_timeseries(hc_td, tapermethod="startend")
            hp_td = taper_pycbc_series(
                hp_td,
                ending_time=35
                * (params_template.m1 + params_template.m2)
                * lal.MTSUN_SI,
            )
            hc_td = taper_pycbc_series(
                hc_td,
                ending_time=35
                * (params_template.m1 + params_template.m2)
                * lal.MTSUN_SI,
            )

        N = max(len(hp_td), len(s))
        pad = int(2 ** (np.floor(np.log2(N)) + 2))
        hp_td.resize(pad)
        hc_td.resize(pad)
        s.resize(pad)
        s_tilde = make_frequency_series(s)

        hp_tilde = make_frequency_series(hp_td)
        hc_tilde = make_frequency_series(hc_td)
    else:
        # Frequency doman template
        # FFT the signal as well, if it's in the time domain
        if isinstance(s, pt.TimeSeries):

            N = len(s)
            pad = int(2 ** (np.floor(np.log2(N)) + 2))
            s.resize(pad)

        s_tilde = make_frequency_series(s)  # If in FD already, this is a no-op.
        f_max = s_tilde.get_sample_frequencies()[-1]

        params_template.delta_f = s_tilde.delta_f
        # print(f"The delta spacing is {s_tilde.delta_f}")
        # Combine modes
        if modes_dict is not None:
            hp_tilde, hc_tilde = combine_modes(
                params_template.iota, params_template.phi, modes_dict
            )
        else:
            hp_tilde, hc_tilde = generate_waveform(params_template, f_max=f_max)

    # Sanity check
    assert len(s_tilde) == len(
        hp_tilde
    ), "The length of template and signal don't match!, {} and {} ".format(
        len(s_tilde),
        len(hp_tilde)
        # , pad, len(hp_td), len(s)
    )

    psd_a = generate_psd(len(s_tilde), s_tilde.delta_f, flow, psd_type=psd_type)
    # print(f"signal delta_f:{s_tilde.delta_f}")
    # print(f"template delta_f:{hp_tilde.delta_f}")
    # print(f"psd delta_f:{psd_a.delta_f}")

    # For debugging only
    # print("phi={}, pycbc mismatch = {}".format(params_template.phi,1-match(s_tilde,hp_tilde,psd=psd_a,low_frequency_cutoff=flow,high_frequency_cutoff=fhigh)[0]))
    # Norm of h_{+}
    A_p = _filter.sigma(
        hp_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    # Norm of h_{x}
    A_c = _filter.sigma(
        hc_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    # Norm of s, |s|
    N_1 = _filter.sigma(
        s_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )

    # Normalized templates
    hp_hat = hp_tilde / A_p
    hc_hat = hc_tilde / A_c

    # Cross term, i.e. (h_{+}|h_{x})
    # Notice that normalized=False, because we do our own normalization
    Ipc = _filter.overlap(
        hp_hat,
        hc_hat,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        normalized=False,
    )

    # Compute the complex SNRs
    # Again, sigmasq=1, don't normalize!
    rho_p_hat = _filter.matched_filter(
        hp_hat,
        s_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        sigmasq=1.0,
    )

    rho_c_hat = _filter.matched_filter(
        hc_hat,
        s_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        sigmasq=1.0,
    )
    rho_p_hat = rho_p_hat.real()
    rho_c_hat = rho_c_hat.real()
    rho_p_hat = rho_p_hat.numpy()
    rho_c_hat = rho_c_hat.numpy()

    num = rho_p_hat**2 + rho_c_hat**2 - 2 * rho_p_hat * rho_c_hat * Ipc
    denum = 1 - Ipc**2
    overlap = np.sqrt(num / denum) * 1 / N_1
    overlap = np.max(overlap)
    # print("mm = {}".format(1 - overlap))

    """
    # Construct the PSD
    psd_a = generate_psd(len(s_tilde), s_tilde.delta_f, flow)

    A_p = _filter.sigma(
        hp_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    A_c = _filter.sigma(
        hc_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )

    N_1 = _filter.sigma(
        s_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )

    Ipc = _filter.overlap(
        hp_tilde,
        hc_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        normalized=False,
    )

    rho_p, _, norm = _filter.matched_filter_core(
        s_tilde,
        hp_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        h_norm=1.0,
    )
    rho_p = (4.0 * s_tilde.delta_f) * rho_p.real()

    rho_c, _, norm = _filter.matched_filter_core(
        s_tilde,
        hc_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        h_norm=1.0,
    )
    rho_c = (4.0 * s_tilde.delta_f) * rho_c.real()
    rho_p = rho_p.numpy()
    rho_c = rho_c.numpy()

    # This is a faster way to compute the overlap.
    # See https://arxiv.org/pdf/1709.09181.pdf, Eq(13)
    # The overlap is given by $\mathcal{O} = \sqrt(\rho^{2})/|s|$
    # This is completely equivalent to the expression below
    # in the debug part, but is faster computationally
    temp1 = rho_p ** 2 * A_c ** 2 + rho_c ** 2 * A_p ** 2 - 2 * rho_p * rho_c * Ipc
    temp2 = A_p ** 2 * A_c ** 2 - Ipc ** 2
    test = 1 / N_1 * np.sqrt(temp1 / temp2)
    overlap = np.max(test)
    """

    if debug:
        Ipc = _filter.overlap(
            hp_tilde,
            hc_tilde,
            psd=psd_a,
            low_frequency_cutoff=flow,
            high_frequency_cutoff=fhigh,
            normalized=False,
        )
        rho_p, _, norm = _filter.matched_filter_core(
            s_tilde,
            hp_tilde,
            psd=psd_a,
            low_frequency_cutoff=flow,
            high_frequency_cutoff=fhigh,
            h_norm=1.0,
        )
        rho_p = (4.0 * s_tilde.delta_f) * rho_p.real()

        rho_c, _, norm = _filter.matched_filter_core(
            s_tilde,
            hc_tilde,
            psd=psd_a,
            low_frequency_cutoff=flow,
            high_frequency_cutoff=fhigh,
            h_norm=1.0,
        )
        rho_c = (4.0 * s_tilde.delta_f) * rho_c.real()
        rho_p = rho_p.numpy()
        rho_c = rho_c.numpy()
        alpha = np.sqrt(rho_p**2 + rho_c**2)  # this is a time series
        kappa0 = np.angle(rho_p + 1.0j * rho_c)  # this is a time series
        gamma = np.sqrt(((A_p**2 - A_c**2) / 2.0) ** 2 + Ipc**2)
        sigma0 = np.angle((A_p**2 - A_c**2) / 2.0 + 1.0j * Ipc)
        beta = (A_p**2 + A_c**2) / 2.0

        num = alpha * np.sqrt(beta - gamma * np.cos(2 * kappa0 - sigma0))
        den = N_1 * np.sqrt(beta**2 - gamma**2)
        overlap = num / den

        idx_max = np.argmax(overlap)
        overlap = np.max(overlap)
        dt = idx_max * s.delta_t
        if idx_max > len(s) / 2:
            dt = dt - s.duration
        psi_max_num = beta * np.sin(kappa0[idx_max]) + gamma * np.sin(
            kappa0[idx_max] - sigma0
        )
        psi_max_denom = beta * np.cos(kappa0[idx_max]) - gamma * np.cos(
            kappa0[idx_max] - sigma0
        )
        psi_max = np.arctan2(psi_max_num, psi_max_denom)
        if psi_max < 0:
            psi_max += 2 * np.pi
        print(
            "Overlap: {},dt: {}, psi_max: {}, f_ref: {}, phi_ref: {}".format(
                overlap, dt, psi_max, params_template.f_ref, params_template.phi
            )
        )
        return 1 - overlap, dt, psi_max
    return 1 - overlap


def unfaithfulness_IH(
    s_arg: Union[pt.TimeSeries, pt.FrequencySeries],
    params_template: waveform_params,
    flow: float = 10.0,
    fhigh: float = None,
    modes_dict: Dict = None,
    debug: bool = False,
):
    """This function returns the overlap maximized over alpha
    for given value of phi_t. See Harry et al, https://arxiv.org/pdf/1603.02444.pdf

    Args:
        s_arg (Union[pt.TimeSeries, pt.FrequencySeries]): The signal in the detector
        params_template (waveform_params): The template parameters
        flow (float, optional): Low freqeuncy cutoff. Defaults to 10.0.
        fhigh (float, optional): High frequency cutoff. Defaults to None.
        modes_dict (Dict, optional): Dictionary of modes. To be used for EOB. Defaults to None.

    Returns:
        float: The unfaithfulness
    """
    s = s_arg.copy()
    if params_template.domain == "TD":
        # If we are not given a dictionary of modes, generate the template from scratch
        if not modes_dict:
            hp_td, hc_td = generate_waveform(params_template)
        else:
            # Combine modes
            hp_t, hc_t = combine_modes(
                params_template.iota, params_template.phi, modes_dict
            )

            # Taper
            hp_td = TimeSeries(hp_t, delta_t=params_template.delta_t)
            hc_td = TimeSeries(hc_t, delta_t=params_template.delta_t)
            hp_td = taper_timeseries(hp_td, tapermethod="startend")
            hc_td = taper_timeseries(hc_td, tapermethod="startend")

        N = max(len(hp_td), len(s))
        pad = int(2 ** (np.floor(np.log2(N)) + 2))
        hp_td.resize(pad)
        hc_td.resize(pad)
        s.resize(pad)
        s_tilde = make_frequency_series(s)
        hp_tilde = make_frequency_series(hp_td)
        hc_tilde = make_frequency_series(hc_td)
    else:
        # Frequency doman template
        # FFT the signal as well, if it's in the time domain
        if isinstance(s, pt.TimeSeries):
            N = len(s)
            pad = int(2 ** (np.floor(np.log2(N)) + 2))
            s.resize(pad)
        s_tilde = make_frequency_series(s)
        f_max = s_tilde.get_sample_frequencies()[-1]
        params_template.delta_f = s_tilde.delta_f
        hp_tilde, hc_tilde = generate_waveform(params_template, f_max=f_max)

    # Sanity check
    assert len(s_tilde) == len(
        hp_tilde
    ), "The length of template and signal don't match!"

    # Construct the PSD
    psd_a = generate_psd(len(s_tilde), s_tilde.delta_f, flow)

    # Norm of h_{+}
    A_p = _filter.sigma(
        hp_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    # Norm of h_{x}
    A_c = _filter.sigma(
        hc_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )
    # Norm of s, |s|
    N_1 = _filter.sigma(
        s_tilde, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh, psd=psd_a
    )

    # Normalized templates
    hp_hat = hp_tilde / A_p
    hc_hat = hc_tilde / A_c

    # Cross term, i.e. (h_{+}|h_{x})
    # Notice that normalized=False, because we do our own normalization
    Ipc = _filter.overlap(
        hp_hat,
        hc_hat,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        normalized=False,
    )

    # Compute the complex SNRs
    # Again, sigmasq=1, don't normalize!
    rho_p_hat = _filter.matched_filter(
        hp_hat,
        s_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        sigmasq=1.0,
    )

    rho_c_hat = _filter.matched_filter(
        hc_hat,
        s_tilde,
        psd=psd_a,
        low_frequency_cutoff=flow,
        high_frequency_cutoff=fhigh,
        sigmasq=1.0,
    )

    gamma_hat = (rho_p_hat * rho_c_hat.conj()).real().numpy()
    rho_p2 = np.abs(rho_p_hat) ** 2
    rho_c2 = np.abs(rho_c_hat) ** 2

    a = Ipc * rho_p2 - gamma_hat
    b = rho_p2 - rho_c2
    c = gamma_hat - Ipc * rho_c2
    pos_root = (-b + np.sqrt(b**2 - 4 * a * c)) / (2 * a)
    psi_max = np.arctan(1 / pos_root * np.sqrt(A_p**2 / A_c**2))
    max_lambda_root = np.sqrt(
        (rho_p2 - rho_c2) ** 2
        + 4.0 * (Ipc * rho_p2 - gamma_hat) * (Ipc * rho_c2 - gamma_hat)
    )
    max_lambda_num = rho_p2 - (2.0 * gamma_hat * Ipc) + rho_c2 + max_lambda_root
    max_lambda_den = 1.0 - Ipc**2

    max_lambda_list = 0.25 * max_lambda_num.numpy() / max_lambda_den
    max_lambda_idx = np.argmax(max_lambda_list)
    max_lambda = max_lambda_list[max_lambda_idx]

    # Recall that $\lambda=\frac{1}{2}\frac{(s|h)^{2}}{|h|^{2}}$
    # On the other hand $\mathcal{O} = \frac{(s|h)}{|s||h|}$. Thus
    # \mathcal{O} = $\sqrt{\frac{2\lambda}{|s|}}$
    overlap = np.sqrt(2 * max_lambda) / N_1

    dt = max_lambda_idx * s.delta_t
    # print("IH: ",1-overlap)
    if max_lambda_idx > len(s) / 2:
        dt = dt - s.duration
    if debug:
        return 1 - overlap, dt, psi_max[max_lambda_idx]
    return 1 - overlap


def interpolate_mode_dict_and_rescale(
    modes_EOB: Dict, t_EOB: np.array, params_template: waveform_params
) -> Dict:
    """Interpolate a set of modes and rescale them to SI units

    Args:
        modes_EOB (Dict): The waveform modes
        t_EOB (np.array): Time in geometric units
        params_template (waveform_params): The parameters of the waveform. Specifies
            the time resolution via delta_t.

    Returns:
        Dict: The waveform modes at the desired timespacing
    """
    dc_rescaled = {}
    t_EOB_rescaled = t_EOB * (params_template.m1 + params_template.m2) * lal.MTSUN_SI
    new_times = np.arange(0, t_EOB_rescaled[-1], params_template.delta_t)
    amp_factor = (
        (params_template.m1 + params_template.m2)
        * lal.MRSUN_SI
        / params_template.distance
    )
    for key in modes_EOB.keys():
        intrp_re = InterpolatedUnivariateSpline(t_EOB_rescaled, np.real(modes_EOB[key]))
        intrp_im = InterpolatedUnivariateSpline(t_EOB_rescaled, np.imag(modes_EOB[key]))

        dc_rescaled[key] = (intrp_re(new_times) + 1j * intrp_im(new_times)) * amp_factor
    return dc_rescaled


def run_unfaithfulness(
    phi_s: float,
    kappa_s: float,
    iota_s: float,
    index: int,
    template_approx: str = "IMRPhenomPv3HM",
    signal_approx: str = "NR_hdf5",
    NR_file: str = None,
    NR_file_2: str = None,
    parameters: Dict = None,
    flow_d: float = 10.0,
    fhigh: float = None,
    ellMax: int = 4,
    debug: bool = False,
    save_file: bool = True,
    unfaithfulness_type: str = "unfaithfulness_RC",
    minimization_type: str = "reference_frequency",
    simple: bool = False,
    fudge_factor: float = 1.35,
    mass_range: tuple = (20.0, 300.0, 20.0),
):
    """Compute the unfaithfulness maximizing over template parameters
    Currently the following combinations are supported:
    1. Signal in time domain and template in time domain
    2. Signal in time domain and template in frequency domain
    3. Signal and template in frequency domain.

    Args:
        phi_s (float): Azimuthal angle of the signal (rad). Ultimately averaged over.
        kappa_s (float): Effective polarization of the signal (rad). Ultimately averaged over.
        iota_s (float): Inclination of the signal (rad)
        index (int): Index to use to uniqualy identify the particular (phi_s,kappa_s) case
        template_approx (str, optional): Template approximant. Defaults to "IMRPhenomPv3HM".
        signal_approx (str, optional): Signal approximant. Defaults to "NR_hdf5".
        NR_file (str, optional): File containing NR data in LVC format. Defaults to None.
        NR_file_2 (str, optional): Second file containing NR data. Defaults to None.
        parameters (dict, optional): If the signal is not NR, the intrinsic parameters of the signal
        fhigh (float, optional): Upper frequency cutoff. Defaults to None.
        flow_d (float,optional): The desired lower bound of SNR computation. Defaults to 10 Hz
        ellMax (int, optional): Maximum ell to use. Defaults to 4.
        debug (bool, optional): Print debug information. Defaults to False.
        save_file (bool, optional): Save the output to a file. Defaults to True.
        unfaithfulness_type (str,optional): Which quantity to minimize. Defaults to "unfaithfulness_RC"
        mimization_type (str, optional): What type of mimization to use. Defaults to "reference_frequency"
        simple (bool,optional): Return a simplistic mismatch (not recommended)
        fudge_factor (float,optional). The buffer fraction to use for the lower bound of mismatch integral
        mass_range (tuple,optional). The mass parameters to use in the form (low,high,step)
    Returns:
        np.array The unfaithfulness
    """

    # Generic bounds on the initial eccentricity  of each model, below we use also the Newtonian estimate at the starting
    # frequency to set a maximum allowed eccentricity
    if template_approx == "TEOBResumSe":
        e0max_template = 0.3
    elif template_approx == "TEOBResumSeHM":
        e0max_template = 0.3
    elif template_approx == "SEOBNREv4":
        e0max_template = 0.7
    elif template_approx == "IMRPhenomXE":
        e0max_template = 0.2
    else:
        e0max_template = 0.0

    # Set the function we are going to minimize
    # Note that this choice does not affect what happens
    # for EOB approximants
    # Use only one minimization function

    min_func = minimize_unfaithfulness

    # Figure out the domains of the signal and the template
    td_approxs = td_approximants()
    fd_approxs = fd_approximants()
    if signal_approx not in fd_approxs or signal_approx == "SEOBNRv4":
        signal_domain = "TD"
        delta_t_signal = 1.0 / 16384
        delta_f_signal = None
    # elif signal_approx == "SEOBNREv4" or signal_approx == "TEOBResumSe" or signal_approx == "TEOBResumSeHM":
    #    signal_domain = "TD"
    #    delta_t_signal = 1.0 / 8192
    #    delta_f_signal = None
    else:
        signal_domain = "FD"
        delta_f_signal = 0.125
        delta_t_signal = None

    # if template_approx == "SEOBNREv4" or template_approx == "TEOBResumSe" or signal_approx == "TEOBResumSeHM":
    #    signal_domain = "TD"
    #    delta_t_signal = 1.0 / 8192
    #    delta_f_signal = None
    # print(template_approx, fd_approxs)

    if template_approx not in fd_approxs or template_approx == "SEOBNRv4":
        template_domain = "TD"
        delta_t_template = 1.0 / 16384
        delta_f_template = None

    else:
        template_domain = "FD"
        delta_f_template = 0.125
        delta_t_template = None

    # print("template_approx = ", template_approx,"     template_domain = ",template_domain)
    approx_type = get_approximant_type(template_approx)
    LAL_params_signal = None
    LAL_params_template = None

    # print("approx type = ", signal_approx ,"    signal domain = ", signal_domain)

    # Check how we are going to get the signal
    if signal_approx == "NR_hdf5":
        if NR_file:
            fp = h5py.File(NR_file, "r")
            freq_1M = fp.attrs["f_lower_at_1MSUN"]
            # Add rounding to prevent some errors in the calculation of the symmetric mass ratio (for TEOBResumSe)
            m1_dim = round(fp.attrs["mass1"], 5)
            m2_dim = round(fp.attrs["mass2"], 5)
            e0_signal = round(fp.attrs["eccentricity"], 4)
            mean_anomaly_signal = round(fp.attrs["mean_anomaly"], 4)

            fp.close()
            pth = os.path.dirname(NR_file)
            pth = pth.replace("/", "_")
            pat = re.compile("SimAnnex_(.*)")
            preprefix = re.findall(pat, pth)[0]
            prefix = f"{preprefix}_{os.path.basename(NR_file).split('.')[0]}"
            if os.path.isfile("{}_result_{:05d}.dat".format(prefix, index)):
                return
            LAL_params_signal = lal.CreateDict()
            ma = lalsim.SimInspiralCreateModeArray()

            # For the models below activate only the (2,2) mode  --- Find a smart way to generate only the 22 mode for non-HoM models ---
            if (
                template_approx == "SEOBNRv4"
                or template_approx == "TEOBResumS"
                or template_approx == "TEOBResumSe"
                or template_approx == "SEOBNREv4"
                or template_approx == "IMRPhenomXE"
                or template_approx == "IMRPhenomD"
                or template_approx == "IMRPhenomXAS"
                or template_approx == "SEOBNRv4_ROM"
            ):
                # print("Activating only the dominant mode")
                lalsim.SimInspiralModeArrayActivateMode(ma, 2, 2)
                lalsim.SimInspiralModeArrayActivateMode(ma, 2, -2)
            else:
                for ell in range(2, ellMax + 1):
                    lalsim.SimInspiralModeArrayActivateAllModesAtL(ma, ell)
            lalsim.SimInspiralWaveformParamsInsertModeArray(LAL_params_signal, ma)
            lalsim.SimInspiralWaveformParamsInsertNumRelData(LAL_params_signal, NR_file)
            (
                s1x,
                s1y,
                s1z,
                s2x,
                s2y,
                s2z,
            ) = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(
                0.0, 100, NR_file
            )  # Since we are using f_ref=0, total mass does not matter

            # print(m1_dim, m2_dim, s1x, s1y, s1z, s2x, s2y, s2z)
        else:
            print(
                "When using the 'NR_hdf5' approximant, you must provide the NR_file option!"
            )
            exit(-1)
    elif signal_approx == "NR_custom" and template_approx == "NR_custom":
        # If we are here, the assumption is that
        # we are compairing 2 NR wfs.
        pth = os.path.dirname(NR_file)
        pth = pth.replace("/", "_")
        pat = re.compile("SimAnnex_(.*)")
        preprefix = re.findall(pat, pth)[0]
        prefix = f"{preprefix}_{os.path.basename(NR_file).split('.')[0]}"
        if os.path.isfile("{}_result_{:05d}.dat".format(prefix, index)):
            return
        (
            modes_template,
            t_template,
            _,
        ) = get_NR_data(NR_file_2, ell_max=ellMax)
        (
            modes_signal,
            t_signal,
            physical_params,
        ) = get_NR_data(NR_file, ell_max=ellMax)
        m1_dim = physical_params["m1_dim"]
        m2_dim = physical_params["m2_dim"]
        s1x, s1y, s1z = physical_params["chi1"]
        s2x, s2y, s2z = physical_params["chi2"]
        freq_1M = physical_params["freq_1M"]
    elif signal_approx == "NR_custom" and template_approx != "NR_custom":
        # If we are here, the assumption is that
        # we are compairing 2 NR wfs.
        pth = os.path.dirname(NR_file)
        pth = pth.replace("/", "_")
        pat = re.compile("SimAnnex_(.*)")
        preprefix = re.findall(pat, pth)[0]
        prefix = f"{preprefix}_{os.path.basename(NR_file).split('.')[0]}"
        if os.path.isfile("{}_result_{:05d}.dat".format(prefix, index)):
            return
        (
            modes_signal,
            t_signal,
            physical_params,
        ) = get_NR_data(NR_file, ell_max=ellMax)
        m1_dim = physical_params["m1_dim"]
        m2_dim = physical_params["m2_dim"]
        s1x, s1y, s1z = physical_params["chi1"]
        s2x, s2y, s2z = physical_params["chi2"]
        freq_1M = physical_params["freq_1M"]
        e0_signal = 0.0
        mean_anomaly_signal = 0.0
    else:
        # We are not using NR as the signal.
        q = parameters["q"]
        s1x, s1y, s1z = parameters["chi1"]
        s2x, s2y, s2z = parameters["chi2"]
        freq_1M = parameters["freq_1M"]
        prefix = parameters["case"]

        if "eccentricity" in parameters:
            e0_signal = parameters["eccentricity"]
        else:
            e0_signal = 0.0
        if "mean_anomaly" in parameters:
            mean_anomaly_signal = parameters["mean_anomaly"]
        else:
            mean_anomaly_signal = 0.0

        m1_dim = q / (1.0 + q)
        m2_dim = 1 - m1_dim
        if os.path.isfile("{}_result_{:05d}.dat".format(prefix, index)):
            return
        if signal_approx == "NRSur7dq4":
            ma = lalsim.SimInspiralCreateModeArray()
            for ell in range(2, ellMax + 1):
                lalsim.SimInspiralModeArrayActivateAllModesAtL(ma, ell)

    Mtotals = np.arange(mass_range[0], mass_range[1] + mass_range[2], mass_range[2])
    # print(f"Mtotals:{Mtotals}")
    dist = 1e6 * lal.PC_SI * 500  # Doesn't matter what this is
    res = []
    # First set the parameters using a feducial total mass
    M_fed = 60.0
    m1 = m1_dim * M_fed
    m2 = m2_dim * M_fed
    f_ref = freq_1M / (m1 + m2)
    f_min = 1 * f_ref

    params_signal = waveform_params(
        m1,
        m2,
        s1x,
        s1y,
        s1z,
        s2x,
        s2y,
        s2z,
        iota_s,
        phi_s,
        f_ref,
        f_min,
        dist,
        delta_t_signal,
        delta_f_signal,
        LAL_params_signal,
        signal_approx,
        signal_domain,
        e0_signal,
        mean_anomaly_signal,
    )

    if approx_type == "aligned":
        if np.abs(s1x) < 1e-3:
            s1x = 0
        if np.abs(s1y) < 1e-3:
            s1y = 0
        if np.abs(s2x) < 1e-3:
            s2x = 0
        if np.abs(s2y) < 1e-3:
            s2y = 0

    params_template = waveform_params(
        m1,
        m2,
        s1x,
        s1y,
        s1z,
        s2x,
        s2y,
        s2z,
        iota_s,
        phi_s,
        f_ref,
        f_min,
        dist,
        delta_t_template,
        delta_f_template,
        LAL_params_template,
        template_approx,
        template_domain,
        e0_signal,
        mean_anomaly_signal,
    )

    # print(params_signal.delta_t , delta_t_signal)
    # print(params_template.delta_t, delta_t_template)
    # params_signal.delta_f = delta_f_signal
    # params_template.delta_f = delta_f_template
    modes_EOB = None
    if template_approx in EOB_approximants:
        # For EOB which supports modes we iterate f_min until we get the same time to merger
        if signal_approx == "NR_hdf5":
            f_min_EOB = iterate_time_to_merger(
                params_signal, params_template, NR_file, ma
            )
        elif signal_approx == "NRSur7dq4":
            f_min_EOB = iterate_time_to_merger(params_signal, params_template, None, ma)
        elif signal_approx == "SEOBNRv4PHM":
            f_min_EOB = iterate_time_to_merger(
                params_signal, params_template, None, None
            )

        """print(
            "The EOB f_min={}, NR f_min = {}".format(
                f_min_EOB * M_fed, params_signal.f_min * M_fed
            )
        )"""
        time_EOB, modes_EOB = get_final_modes(f_min_EOB, params_template, ellMax)

    # if template_approx == "SEOBNRv5HM":
    #    time_EOB,modes_EOB = get_EOB_modes(params_template)
    # if template_approx == "TEOBResumSHM":
    #    time_EOB,modes_EOB = get_EOB_modes_TEOB(params_template)
    # Is this condition necessary for TD eccentric approxs ?
    # Eccentricity varies the length of the waveforms. Should we impose that both the signal and the template have the same time from t0 to t_peak  ?
    # Currently we are doing nothing with f_min_EOB
    if template_approx in eEOB_approximants:
        # print("signal = ", signal_approx)
        # print("template = ", template_approx)

        if signal_approx == "NR_hdf5":
            f_min_EOB = iterate_time_to_merger(
                params_signal, params_template, NR_file, ma
            )

        elif signal_approx == "NRSur7dq4":
            f_min_EOB = iterate_time_to_merger(params_signal, params_template, None, ma)

        elif signal_approx == "SEOBNRv4PHM":
            f_min_EOB = iterate_time_to_merger(
                params_signal, params_template, None, None
            )
        # elif signal_approx == "TEOBResumSe" or signal_approx == "TEOBResumSeHM" or signal_approx == "SEOBNREv4":
        else:
            f_min_EOB = iterate_time_to_merger(
                params_signal, params_template, None, None
            )

        """print(
            "The EOB f_min={}, signal f_min = {}".format(
                f_min_EOB * M_fed, params_signal.f_min * M_fed
            )
        )"""

        """
        if p.approx == "TEOBResumSe":
                t, htp, htc,amp22 = generate_TEOBResumSe_simple_waveform(params_template, f_max,modesTEOB)

        elif p.approx == "TEOBResumSe" or p.approx == "SEOBNREv4":
                t, htp, htc, amp22 = generate_SEOBNREv4_waveform(params_template, f_max)

        time_EOB = params_template.delta_t * np.arange(len(htp)) / ((params_template.m1 + params_template.m2) * lal.MTSUN_SI)
        """
    for M_tot in Mtotals:

        m1 = m1_dim * M_tot
        m2 = m2_dim * M_tot

        f_min = freq_1M / (m1 + m2)
        f_ref = f_min
        flow = fudge_factor * f_min
        if flow < flow_d:
            flow = flow_d

        # Check consistency of eccentricity in the templates

        if e0_signal > 0.0:  # signal is eccentric!
            e0max_signal = e0Newt(f_min, e0_signal, f_ref)
        else:
            e0max_signal = 0.0

        if e0max_template > 0.0:  # template is eccentric!
            eNewt_template = e0Newt(f_min, e0max_template, f_ref)
            if e0max_template > eNewt_template:
                e0max_template = eNewt

        if e0_signal == 0.0:
            e0max_template = 0.1  # Set it to smaller value, such that it does not take too long to compute

        # Update the params
        params_template.m1 = m1
        params_template.m2 = m2
        params_signal.m1 = m1
        params_signal.m2 = m2

        params_signal.f_min = f_min
        params_signal.f_ref = f_ref
        params_template.f_min = f_min
        params_template.f_ref = f_ref

        if signal_approx == "NR_custom":
            modes_resc = interpolate_mode_dict_and_rescale(
                modes_signal, t_signal, params_signal
            )
            sp, sc = combine_modes(iota_s, phi_s, modes_resc)
            # Taper
            # print(modes_signal['2,2'],t_signal)
            # print(sp,params_signal.delta_t)
            sp_td = TimeSeries(sp, delta_t=params_signal.delta_t)
            sc_td = TimeSeries(sc, delta_t=params_signal.delta_t)
            # sp_td = taper_timeseries(sp_td, tapermethod="startend")
            # sc_td = taper_timeseries(sc_td, tapermethod="startend")
            sp_td = taper_pycbc_series(
                sp_td,
                ending_time=35 * (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI,
            )
            sc_td = taper_pycbc_series(
                sc_td,
                ending_time=35 * (params_signal.m1 + params_signal.m2) * lal.MTSUN_SI,
            )
            s = sp_td * np.cos(kappa_s) + sc_td * np.sin(kappa_s)
        else:
            sp, sc = generate_waveform(params_signal)
            s = sp * np.cos(kappa_s) + sc * np.sin(kappa_s)

        if isinstance(s, pt.TimeSeries):
            N = len(s)
            pad = int(2 ** (np.floor(np.log2(N)) + 2))
            s.resize(pad)
        s_tilde = make_frequency_series(s)
        psd = generate_psd(len(s_tilde), s_tilde.delta_f, flow)
        SNR = _filter.sigma(
            s_tilde, psd=psd, low_frequency_cutoff=flow, high_frequency_cutoff=fhigh
        )
        if template_approx in EOB_approximants:
            # We have to interpolate the modes to the correct time spacing and
            # rescale them
            modes_dc = interpolate_mode_dict_and_rescale(
                modes_EOB, time_EOB, params_template
            )

            # To avoid local minima, just brute force in 1D
            # Use a 100 point grid
            """
            final = brute(
                min_func,
                ((0, 2 * np.pi),),
                args=(
                    s,
                    params_template,
                    kappa_s,
                    flow,
                    fhigh,
                    unfaithfulness_type,
                    modes_dc,
                    debug,
                ),
                Ns=100,
                finish=optimize.fmin,
                full_output=True,
            )

            final = minimize(min_func,params_signal.phi,
                                    args=(
                                                            s,
                                                            params_template,
                                                            kappa_s,
                                                            flow,
                                                            fhigh,
                                                            unfaithfulness_type,
                                                            modes_dc,
                                                            debug
                                                        ),method='Nelder-Mead')
            """
            final = dual_annealing(
                min_func,
                ((0, 2 * np.pi),),
                args=(
                    s,
                    params_template,
                    kappa_s,
                    flow,
                    fhigh,
                    unfaithfulness_type,
                    modes_dc,
                    debug,
                    "phi",
                ),
                local_search_options={"method": "Nelder-Mead"},
                maxfun=500,
            )
        elif signal_approx == "NR_custom" and template_approx == "NR_custom":
            modes_dc = interpolate_mode_dict_and_rescale(
                modes_template, t_template, params_template
            )
            final = dual_annealing(
                min_func,
                ((0, 2 * np.pi),),
                args=(
                    s,
                    params_template,
                    kappa_s,
                    flow,
                    fhigh,
                    unfaithfulness_type,
                    modes_dc,
                    debug,
                    "phi",
                ),
                local_search_options={"method": "Nelder-Mead"},
                maxfun=500,
            )
        else:
            modes_dc = None
            # We need to avoid local minima
            # Can use dual annealing.
            # However, in 2D it's easier to just brute force
            # Use a 30x30 grid
            if minimization_type == "phi":
                bounds = ((0, 2 * np.pi),)
            elif minimization_type == "reference_frequency":
                bounds = ((0.8 * f_ref, 1.4 * f_ref), (0, 2 * np.pi))
            elif minimization_type == "spin_rotation":
                bounds = ((0, 2 * np.pi), (0, 2 * np.pi))

            elif minimization_type == "eccentricity":
                bounds = ((0, 2 * np.pi), (0, e0max_template))

            elif minimization_type == "eccentricity_22mode":
                bounds = ((0, e0max_template),)

            elif minimization_type == "eccentricity_meanAnomaly":
                bounds = ((0, 2 * np.pi), (0, e0max_template), (0, 2 * np.pi))

            elif minimization_type == "eccentricity_reference_frequency":
                bounds = (
                    (0, 2 * np.pi),
                    (0, e0max_template),
                    (0.8 * f_ref, 1.4 * f_ref),
                )

            elif minimization_type == "eccentricity_spin_rotation":
                bounds = ((0, 2 * np.pi), (0, e0max_template), (0, 2.0 * np.pi))

            elif minimization_type == "eccentricity_mean_anomaly_reference_frequency":
                bounds = (
                    (0, 2 * np.pi),
                    (0, e0max_template),
                    (0, 2 * np.pi),
                    (0.8 * f_ref, 1.4 * f_ref),
                )

            elif minimization_type == "eccentricity_mean_anomaly_spin_rotation":
                bounds = (
                    (0.0, 2.0 * np.pi),
                    (0, e0max_template),
                    (0, 2 * np.pi),
                    (0, 2 * np.pi),
                )

            else:
                raise NotImplementedError

            """
            final = brute(
                min_func,
                bounds,
                args=(
                    s,
                    params_template,
                    kappa_s,
                    flow,
                    fhigh,
                    unfaithfulness_type,
                    modes_dc,
                    debug,
                ),
                Ns=30,
                finish=optimize.fmin,
                full_output=True
            )
            """
            if approx_type == "precessing":
                final = dual_annealing(
                    min_func,
                    bounds,
                    args=(
                        s,
                        params_template,
                        kappa_s,
                        flow,
                        fhigh,
                        unfaithfulness_type,
                        modes_dc,
                        debug,
                        minimization_type,
                    ),
                    local_search_options={"method": "Nelder-Mead"},
                    maxfun=2000,
                )
            elif approx_type == "aligned":
                if modes_EOB:
                    modes_dc = interpolate_mode_dict_and_rescale(
                        modes_EOB, time_EOB, params_template
                    )
                # We are dealing with algined spin
                # Thus we optimize *only* over the phase numerically
                # This means that the type of min_func is irrelevant
                if simple:

                    final = minimize_scalar(
                        min_func,
                        bounds=(0, 2 * np.pi),
                        args=(
                            s,
                            params_template,
                            kappa_s,
                            flow,
                            fhigh,
                            unfaithfulness_type,
                            modes_dc,
                            debug,
                            "phi",
                        ),
                    )
                else:
                    x, fval, _, _ = brute(
                        min_func,
                        ((0, 2 * np.pi),),
                        Ns=200,
                        args=(
                            s,
                            params_template,
                            kappa_s,
                            flow,
                            fhigh,
                            unfaithfulness_type,
                            modes_dc,
                            debug,
                            "phi",
                        ),
                        full_output=True,
                        # finish=optimize.fmin,
                    )
                    final = argparse.Namespace()
                    final.x = x
                    final.fun = fval
            else:
                raise TypeError
        # Return the polished result, as well as the minimum on the grid
        print(f"final: x={final.x},value={final.fun}")
        res.append([M_tot, final.fun, *final.x, SNR])

    res = np.array(res)
    # print(res)
    if save_file:
        np.savetxt("{}_result_{:05d}.dat".format(prefix, index), res)
    # return res
