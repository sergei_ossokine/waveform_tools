#!/usr/bin/env python3
import argparse
import numpy as np
import pandas as pd
from unfaithfulness import run_unfaithfulness
from joblib import Parallel, delayed


def compare_waveforms(
    phi_s: float,
    kappa_s: float,
    iota_s: float,
    random_params: pd.Series,
    signal_approx: str,
    template_approx: str,
    index: int,
    offset: int,
    ell_max: int = 4
):

    # This will hold the info needed by run_unfaithfulness
    dc = dict(q=random_params['q'],chi1=random_params[['chi1x','chi1y','chi1z']],
        chi2=random_params[['chi2x','chi2y','chi2z']],freq_1M=random_params['freq_1M'])
    name = "{}_vs_{}_{:05d}".format(signal_approx, template_approx,offset)
    dc.update(case=name)
    run_unfaithfulness(
        phi_s,
        kappa_s,
        iota_s,
        index,
        template_approx=template_approx,
        signal_approx=signal_approx,
        parameters=dc,
        ellMax = ell_max
    )

if __name__ == "__main__":
    p = argparse.ArgumentParser()
    p.add_argument("--parameter_file",type=str,help="The file containing the physical parameters")
    p.add_argument("--signal_approx",type=str,help="The signal approximant")
    p.add_argument("--template_approx", type=str,help="The template approximant")
    p.add_argument("--offset",type=int,help="The global case number")
    p.add_argument("--iota_s",type=float,help="The inclination angle of the source (rad)",default=np.pi/3)
    p.add_argument("--ell_max",type=int,help="The max ell to use",default=4)

    args = p.parse_args()

    df = pd.read_csv(args.parameter_file)
    random_params = df.iloc[args.offset]
    phis = np.linspace(0, 2 * np.pi, 8, endpoint=False)
    kappas = np.linspace(0, 2*np.pi, 8, endpoint=False)
    x, y = np.meshgrid(kappas, phis)
    x = x.flatten()
    y = y.flatten()
    Parallel(n_jobs=16, verbose=50, backend="multiprocessing")(
        delayed(compare_waveforms)(
            y[i],
            x[i],
            args.iota_s,
            random_params,
            args.signal_approx,
            args.template_approx,
            i,
            args.offset,
            ell_max = args.ell_max
        )
        for i in range(len(x))
    )
  
