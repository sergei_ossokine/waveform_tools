#!/usr/bin/env python3
import sys, os

filepath = os.path.abspath(os.getcwd())
# sys.path.append('/home/antoniramosbuades/git/waveform_tools_eccentric/waveform_tools/mismatch/')
print(filepath + "/../mismatch/")
sys.path.append(filepath + "/../mismatch/")

# import waveform_tools
# from waveform_tools.mismatch import *
# from waveform_tools.mismatch.unfaithfulness import *
# from ...mismatch.unfaithfulness import generate_waveform
from unfaithfulness import *

# from .waveform_parameters import waveform_params

from auxillary_funcs import *
from waveform_parameters import waveform_params

import pycbc.types as pt
import numpy as np
import pytest

np.random.seed(seed=150914)


@pytest.fixture()
def signal_params():
    signal_params = waveform_params(
        m1=160.0,
        m2=40.0,
        s1x=-0.0,
        s1y=0.0,
        s1z=0.3653773707568446,
        s2x=-0.0,
        s2y=0.0,
        s2z=0.73438766171593375,
        iota=0.49631870549205614,
        phi=5.007096838781735,
        f_ref=5.972880105149813,
        f_min=5.972880105149813,
        distance=1.5428387907456836e25,
        delta_t=6.103515625e-05,
        delta_f=None,
        wf_param=None,
        approx="TEOBResumSeHM",
        domain="TD",
        ecc=0.3,
    )
    return signal_params


@pytest.fixture
def signal(signal_params):

    iota_s = np.random.uniform(0, np.pi)
    phi_s = np.random.uniform(low=0.0, high=2 * np.pi)
    kappa_s = np.random.uniform(low=0.0, high=2 * np.pi)
    index = 0
    signal_params.iota = iota_s
    signal_params.phi = phi_s

    sp, sc = generate_waveform(signal_params)
    s = sp * np.cos(kappa_s) + sc * np.sin(kappa_s)
    return s, kappa_s


@pytest.mark.parametrize(
    "unfaithfulness_type", ["unfaithfulness_RC", "unfaithfulness_IH"]
)
def test_trivially_correct(signal, signal_params, unfaithfulness_type):
    s, kappa_s = signal
    if unfaithfulness_type == "unfaithfulness_RC":
        unfaith, dt, kappa_max = unfaithfulness_RC(s, signal_params, debug=True)
    elif unfaithfulness_type == "unfaithfulness_IH":
        unfaith, dt, kappa_max = unfaithfulness_IH(s, signal_params, debug=True)
    else:
        raise NotImplementedError
    assert np.abs(unfaith) < 1e-14, "Unfaithfulness should be zero"
    assert np.abs(dt) < 1e-14, "Timeshift should be zero"
    # This test fails for the other type of unfaithfulness,
    # which indicates that the interpretation of parameters coming out of
    # the definition is not obvious at all.
    if unfaithfulness_type == "unfaithfulness_RC":
        assert (
            np.abs(kappa_s - kappa_max) < 1e-3
        ), "Polarization angle should be correct"


@pytest.mark.parametrize("timeshift", [1.0, 2.5, 3.1])
def test_timeshift(signal, signal_params, timeshift):
    s, kappa_s = signal
    s2 = pt.TimeSeries(s.numpy(), delta_t=s.delta_t)
    s2.prepend_zeros(int(timeshift * 16384))  # Prepend a few seconds of zeros
    unfaith, dt, kappa_max = unfaithfulness_RC(s2, signal_params, debug=True)
    assert np.abs(unfaith) < 1e-14, "Unfaithfulness should be zero"
    assert np.abs(np.abs(dt) - timeshift) < 1e-3, "Wrong timeshift"
    assert np.abs(kappa_s - kappa_max) < 1e-3, "Polarization angle should be correct"


def test_reference_freq_phase():
    pass
