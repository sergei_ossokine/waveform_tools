import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="WaveformTools", # Replace with your own username
    version="0.0.1",
    author="Serguei Ossokine",
    author_email="serguei.ossokine@aei.mpg.de",
    description="A package for performing common tasks with gravitational-wave waveforms.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://hypatia.aei.mpg.de/~sossokine/LVC/waveform_tools/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    install_requires = ["pandas",
                        "numpy",
                        "loguru",
                        "PyCBC",
                        "lalsuite"
                        "h5py",
                        "scipy",
                        "joblib",
                        "sxs"],
    entry_points={
        'console_scripts': 
        ['run_one_case=waveform_tools.mismatch.run_one_case:main',
         'assemble_every_case=waveform_tools.mismatch.assemble_every_case:main'
     ]
    },
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
)
